# - Find libelf include dirs and libraries
# Use this module by invoking find_package with the form:
# find_package(LIBELF [REQUIRED]) # Fail with error if libelf is not found
# This module finds libelf headers and library.
# Results are reported in variables:
# LIBELF_FOUND - True if headers and requested libraries were found
# LIBELF_INCLUDE_DIRS - libelf include directories
# LIBELF_LIBRARIES - libelf component libraries to be linked
# -------------------------------------------------------------------------------------

if (LIBOPCODES_LIBRARIES AND LIBOPCODES_INCLUDE_DIRS)
  set (LibOpcodes_FIND_QUIETLY TRUE)
endif (LIBOPCODES_LIBRARIES AND LIBOPCODES_INCLUDE_DIRS)

unset(_inc_env)
string(REPLACE ":" ";" _path_env "$ENV{include}")
list(APPEND _inc_env "${_path_env}")
string(REPLACE ":" ";" _path_env "$ENV{C_include_PATH}")
list(APPEND _inc_env "${_path_env}")
string(REPLACE ":" ";" _path_env "$ENV{CPATH}")
list(APPEND _inc_env "${_path_env}")
string(REPLACE ":" ";" _path_env "$ENV{include_PATH}")
list(APPEND _inc_env "${_path_env}")
string(REPLACE ":" ";" _path_env "$ENV{PATH}")
list(APPEND _inc_env "${_path_env}")
list(REMOVE_DUPLICATES _inc_env)

find_path (LIBOPCODES_INCLUDE_DIRS
NAMES opcodes.h
PATHS
${_inc_env}
/usr/include
/usr/include/libelf
/usr/local/include
/usr/local/include/libelf
/opt/local/include
/opt/local/include/libelf
ENV CPATH)

## Search library in user defined path (via LD_LIBRARY_PATH and LIBRARY_PATH)
unset(_lib_env)
string(REPLACE ":" ";" _lib_env "$ENV{LD_LIBRARY_PATH}")
list(APPEND _lib_env "${_path_env}")
string(REPLACE ":" ";" _lib_env "$ENV{LIBRARY_PATH}")
list(APPEND _lib_env "${_path_env}")
string(REPLACE ":" ";" _lib_env "$ENV{MIC_LD_LIBRARY_PATH}")
list(APPEND _lib_env "${_path_env}")
list(REMOVE_DUPLICATES _lib_env)

find_library (
LIBOPCODES_LIBRARIES
NAMES opcodes
HINTS ${_lib_env}
NO_DEFAULT_PATH
)

## Search in standard path
if (NOT LIBOCODES_LIBRARIES)
  unset(_lib_env)
  list(APPEND _lib_env "/usr/local/lib64")
  list(APPEND _lib_env "/usr/lib64")
  list(APPEND _lib_env "/usr/local/lib")
  list(APPEND _lib_env "/usr/lib")

  find_library (
    LIBOPCODES_LIBRARIES
    NAMES opcodes
    PATHS ${_lib_env}
    )
endif()

include (FindPackageHandleStandardArgs)

# handle the QUIETLY and REQUIRED arguments and set LIBBFD_FOUND to TRUE if all listed variables are TRUE
FIND_PACKAGE_HANDLE_STANDARD_ARGS(LibOpcodes DEFAULT_MSG
LIBOPCODES_LIBRARIES
LIBOPCODES_INCLUDE_DIRS
)

mark_as_advanced(LIBOPCODES_INCLUDE_DIRS LIBOPCODES_LIBRARIES)
