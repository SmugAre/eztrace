When running eztrace_stats on a set of traces of an MPI application, EZTrace prints various statistics on the MPI messages (average size of messages, time spent waiting for an incoming message, etc.)

eztrace_stats also create a set of files that depict the behavior of the application. These files are stored in /tmp/eztrace_stats_USER by default (the output directory can be specified by passing the -o option to eztrace_stats)

The following files are created:
- communication_matrix.nb_messages.dat
  This file contains the communication matrix with the number of messages sent from an MPI process to the other processes.
- communication_matrix.message_size.dat
  This file contains the communication matrix with the total size of messages sent from an MPI process to the other processes.
- message_size.dat
  This file contains the distribution of message size
- message_size.gp
  This GNUplot script exploits message_size.dat


Additionnaly, you can ask eztrace_stats to create a file that contains the list of messages that were exchanged.
This can be done by setting EZTRACE_MPI_DUMP_MESSAGES to 1. After running eztrace_stats, the file ${USER}_eztrace_message_dump is created.
Here is an example of such file:
--------
# src   dest    len     tag     Isend_tick      SWait_tick      Wait_done_tick  Irecv_tick      RWait_tick      Wait_done_tick  Sender_request  Receiver_request
13      0       61440   1b58    115542014654    115542048379    115626670523    115541830089    115542030875    115848696986    0x296b540       0x36f4cc8
7       0       61440   1770    115542396436    115542398916    115625999773    115541829660    115542030702    115848696830    0x33d43e8       0x36f4ca0
--------

Each line correspond to a message that was exchanged. The columns correspond to:
- src: the rank of the MPI process that sent the message
- dest: the rank of the MPI process that received the message
- len: the length (in byte) of the message
- tag: the MPI tag (in hexadecimal) that was used
- Isend_tick: the date (in nanosecond) when the sending process called MPI_Isend (or entered MPI_Send)
- SWait_tick: the date (in nanosecond) when the sending process called MPI_Wait (or entered MPI_Send)
- Wait_done_tick: the date (in nanosecond) when the sending process left MPI_Wait (or left MPI_Send)
- Irecv_tick: the date (in nanosecond) when the receiving process called MPI_Irecv (or entered MPI_Recv)
- Rwait_tick: the date (in nanosecond) when the receiving process called MPI_Wait (or entered MPI_Recv)
- Wait_done_tick: the date (in nanosecond) when the receiving process left MPI_Wait (or left MPI_Recv)
- Sender_request: the MPI_Request that was passed to MPI_Isend
- Receiver_request: the MPI_Request that was passed to MPI_Irecv