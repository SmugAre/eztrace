file(REMOVE_RECURSE
  "CMakeFiles/libGTG"
  "CMakeFiles/libGTG-complete"
  "gtg/src/libGTG-stamp/libGTG-install"
  "gtg/src/libGTG-stamp/libGTG-mkdir"
  "gtg/src/libGTG-stamp/libGTG-download"
  "gtg/src/libGTG-stamp/libGTG-update"
  "gtg/src/libGTG-stamp/libGTG-patch"
  "gtg/src/libGTG-stamp/libGTG-configure"
  "gtg/src/libGTG-stamp/libGTG-build"
)

# Per-language clean rules from dependency scanning.
foreach(lang)
  include(CMakeFiles/libGTG.dir/cmake_clean_${lang}.cmake OPTIONAL)
endforeach()
