file(REMOVE_RECURSE
  "CMakeFiles/libOpari2"
  "CMakeFiles/libOpari2-complete"
  "opari2/src/libOpari2-stamp/libOpari2-install"
  "opari2/src/libOpari2-stamp/libOpari2-mkdir"
  "opari2/src/libOpari2-stamp/libOpari2-download"
  "opari2/src/libOpari2-stamp/libOpari2-update"
  "opari2/src/libOpari2-stamp/libOpari2-patch"
  "opari2/src/libOpari2-stamp/libOpari2-configure"
  "opari2/src/libOpari2-stamp/libOpari2-build"
)

# Per-language clean rules from dependency scanning.
foreach(lang)
  include(CMakeFiles/libOpari2.dir/cmake_clean_${lang}.cmake OPTIONAL)
endforeach()
