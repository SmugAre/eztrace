/* -*- c-file-style: "GNU" -*- */
/*
 * Copyright (C) CNRS, INRIA, Universite Bordeaux 1, Telecom SudParis
 * See COPYING in top-level directory.
 */

#ifndef EZTRACE_CONVERT_MPI_COLL_H
#define EZTRACE_CONVERT_MPI_COLL_H

#include <stdio.h>
#include "mpi_ev_codes.h"
#include "eztrace_convert.h"
#include "eztrace_convert_mpi.h"
#include "eztrace_hierarchical_array.h"

struct mpi_pending_coll_comm {
  struct ezt_list_token_t token;
  struct mpi_coll_msg_t *msg;
};

struct coll_msg_event {
  uint64_t time;
  int my_rank;
  struct mpi_coll_msg_t *msg;
};


#define EZTRACE_MPI_COLL_ID (EZTRACE_MPI_PREFIX | 0x1001)
#define EZTRACE_MPI_COLL_ENTER_ID (EZTRACE_MPI_PREFIX | 0x1010)
#define EZTRACE_MPI_COLL_LEAVE_ID (EZTRACE_MPI_PREFIX | 0x1011)


#define EZTRACE_MPI_STATS_COLL_ID (EZTRACE_MPI_PREFIX | 0x1100)
#define EZTRACE_MPI_STATS_COLL_MSG_ID (EZTRACE_MPI_PREFIX | 0x1101)

void init_mpi_coll_messages();

void print_coll_stats();

void __print_coll_message(FILE*stream, struct mpi_coll_msg_t *msg);
void __print_coll_messages_recurse(FILE*stream, unsigned depth,
                                   p_eztrace_container p_cont);
void __print_coll_messages(FILE*stream);
void ezt_mpi_dump_coll_messages();

struct mpi_coll_msg_t*

__enter_coll(uint64_t time,
	     enum coll_type_t type,
	     struct ezt_mpi_comm *comm,
	     int my_rank,
	     int data_size,
	     struct mpi_request *req,
	     char* thread_id);

struct mpi_coll_msg_t*
__leave_coll(uint64_t time,
	     enum coll_type_t type,
	     struct ezt_mpi_comm *comm,
	     int my_rank,
	     struct mpi_request *req,
	     char* thread_id);

#endif	/* EZTRACE_CONVERT_MPI_COLL_H */
