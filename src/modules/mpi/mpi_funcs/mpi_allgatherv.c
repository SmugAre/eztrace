/* -*- c-file-style: "GNU" -*- */
/*
 * Copyright (C) CNRS, INRIA, Universite Bordeaux 1, Telecom SudParis
 * See COPYING in top-level directory.
 */

#define _REENTRANT

#include <unistd.h>
#include <stdint.h>
#include <stdio.h>
#include <stdlib.h>
#include <sys/time.h>
#include <sys/timeb.h>
#include <dlfcn.h>
#include <string.h>

#include "mpi.h"
#include "mpi_eztrace.h"
#include "mpi_ev_codes.h"
#include "eztrace.h"

static void MPI_Allgatherv_prolog (CONST void *sendbuf __attribute__((unused)),
				   int sendcount,
				   MPI_Datatype sendtype,
				   void *recvbuf __attribute__((unused)),
				   CONST int *recvcounts __attribute__((unused)),
				   CONST int *displs __attribute__((unused)),
				   MPI_Datatype recvtype,
				   MPI_Comm comm)
{
  int rank = -1;
  int size = -1;
  libMPI_Comm_size(comm, &size);
  libMPI_Comm_rank(comm, &rank);

  int dsize;
  /* retrieve the size of the datatype so that we can compute the message length */
  MPI_Type_size(sendtype, &dsize);
  int data_size = sendcount * dsize;

  EZTRACE_EVENT_PACKED_4(EZTRACE_MPI_START_Allgatherv, (app_ptr)comm, size, rank, data_size);
}

static int MPI_Allgatherv_core (CONST void *sendbuf __attribute__((unused)),
				int sendcount,
				MPI_Datatype sendtype,
				void *recvbuf __attribute__((unused)),
				CONST int *recvcounts __attribute__((unused)),
				CONST int *displs __attribute__((unused)),
				MPI_Datatype recvtype,
				MPI_Comm comm)
{
  return libMPI_Allgatherv(sendbuf, sendcount, sendtype, recvbuf, recvcounts,
                           displs, recvtype, comm);
}
static void MPI_Allgatherv_epilog (CONST void __attribute__((unused))  *sendbuf,
				   int sendcount,
				   MPI_Datatype sendtype,
				   void __attribute__((unused)) *recvbuf,
				   CONST int __attribute__((unused)) *recvcounts,
				   CONST int __attribute__((unused)) *displs,
				   MPI_Datatype recvtype,
				   MPI_Comm comm)
{
  int rank = -1;
  int size = -1;
  libMPI_Comm_size(comm, &size);
  libMPI_Comm_rank(comm, &rank);

  EZTRACE_EVENT_PACKED_3(EZTRACE_MPI_STOP_Allgatherv, (app_ptr)comm, size, rank);
}

int MPI_Allgatherv (CONST void *sendbuf, int sendcount, MPI_Datatype sendtype,
		    void *recvbuf, CONST int *recvcounts, CONST int *displs,
                    MPI_Datatype recvtype, MPI_Comm comm)
{
  FUNCTION_ENTRY;
  MPI_Allgatherv_prolog(sendbuf, sendcount, sendtype, recvbuf, recvcounts,
                        displs, recvtype, comm);
  int ret = MPI_Allgatherv_core(sendbuf, sendcount, sendtype, recvbuf,
                                recvcounts, displs, recvtype, comm);
  MPI_Allgatherv_epilog(sendbuf, sendcount, sendtype, recvbuf, recvcounts,
                        displs, recvtype, comm);
  return ret;
}

void mpif_allgatherv_(void *sbuf, int *scount, MPI_Fint *sd, void *rbuf,
                      int *rcount, int *displs, MPI_Fint *rd, MPI_Fint *c,
                      int *error) {
  FUNCTION_ENTRY;
  MPI_Datatype c_stype = MPI_Type_f2c(*sd);
  MPI_Datatype c_rtype = MPI_Type_f2c(*rd);
  MPI_Comm c_comm = MPI_Comm_f2c(*c);
  void *c_sbuf = CHECK_MPI_IN_PLACE(sbuf);
  void *c_rbuf = CHECK_MPI_IN_PLACE(rbuf);

  MPI_Allgatherv_prolog(c_sbuf, *scount, c_stype, c_rbuf, rcount, displs, c_rtype,
                        c_comm);
  *error = MPI_Allgatherv_core(c_sbuf, *scount, c_stype, c_rbuf, rcount, displs,
                               c_rtype, c_comm);
  MPI_Allgatherv_epilog(c_sbuf, *scount, c_stype, c_rbuf, rcount, displs, c_rtype,
                        c_comm);

}
