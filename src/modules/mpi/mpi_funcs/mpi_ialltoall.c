/* -*- c-file-style: "GNU" -*- */
/*
 * Copyright © CNRS, INRIA, Université Bordeaux 1, Telecom SudParis
 * See COPYING in top-level directory.
 */

#define _REENTRANT

#include <unistd.h>
#include <stdint.h>
#include <stdio.h>
#include <stdlib.h>
#include <sys/time.h>
#include <sys/timeb.h>
#include <dlfcn.h>
#include <string.h>

#include "mpi.h"
#include "mpi_eztrace.h"
#include "mpi_ev_codes.h"
#include "eztrace.h"


#ifdef USE_MPI3
static void MPI_Ialltoall_prolog (CONST void *sendbuf __attribute__((unused)),
				  int sendcount __attribute__((unused)),
				  MPI_Datatype sendtype,
				  void *recvbuf __attribute__((unused)),
				  int recvcnt __attribute__((unused)),
				  MPI_Datatype recvtype,
				  MPI_Comm comm,
				  MPI_Request *r) {
  int rank = -1;
  int size = -1;
  libMPI_Comm_size(comm, &size);
  libMPI_Comm_rank(comm, &rank);

  int ssize, rsize;
  MPI_Type_size(sendtype, &ssize);
  MPI_Type_size(recvtype, &rsize);
  int data_size = sendcount * ssize;

  EZTRACE_EVENT_PACKED_5(EZTRACE_MPI_IALLTOALL, (app_ptr)comm, size, rank, (app_ptr)r, data_size);
}


static int MPI_Ialltoall_core (CONST void *sendbuf, int sendcount, MPI_Datatype sendtype,
			       void *recvbuf, int recvcnt, MPI_Datatype recvtype,
			       MPI_Comm comm,
			       MPI_Request *r)
{
  return libMPI_Ialltoall (sendbuf, sendcount, sendtype, recvbuf, recvcnt, recvtype, comm, r);
}

static void MPI_Ialltoall_epilog (CONST void *sendbuf __attribute__((unused)),
				  int sendcount __attribute__((unused)),
				  MPI_Datatype sendtype __attribute__((unused)),
				  void *recvbuf __attribute__((unused)),
				  int recvcnt __attribute__((unused)),
				  MPI_Datatype recvtype __attribute__((unused)),
				  MPI_Comm comm __attribute__((unused)),
				  MPI_Request *r) {
  EZTRACE_EVENT_PACKED_1(EZTRACE_MPI_STOP_IALLTOALL, (app_ptr)r);
}


int MPI_Ialltoall (CONST void *sendbuf, int sendcount, MPI_Datatype sendtype,
                  void *recvbuf, int recvcnt, MPI_Datatype recvtype,
		   MPI_Comm comm, MPI_Request *r)
{
  FUNCTION_ENTRY;

  MPI_Ialltoall_prolog (sendbuf, sendcount, sendtype, recvbuf, recvcnt, recvtype, comm, r);
  int ret = MPI_Ialltoall_core (sendbuf, sendcount, sendtype, recvbuf, recvcnt, recvtype, comm, r);
  MPI_Ialltoall_epilog (sendbuf, sendcount, sendtype, recvbuf, recvcnt, recvtype, comm, r);

  return ret;
}


void mpif_ialltoall_(void *sbuf, int *scount, MPI_Fint *sd,
		    void *rbuf, int *rcount, MPI_Fint *rd,
		     MPI_Fint *c, MPI_Fint *r, int *error)
{
  FUNCTION_ENTRY;
  MPI_Datatype c_stype = MPI_Type_f2c(*sd);
  MPI_Datatype c_rtype = MPI_Type_f2c(*rd);
  MPI_Comm c_comm = MPI_Comm_f2c(*c);
  MPI_Request c_req = MPI_Request_f2c(*r);

  MPI_Ialltoall_prolog(sbuf, *scount, c_stype, rbuf, *rcount, c_rtype, c_comm, r);
  *error = MPI_Ialltoall_core(sbuf, *scount, c_stype, rbuf, *rcount, c_rtype, c_comm, &c_req);
  *r = MPI_Request_c2f(c_req);
  MPI_Ialltoall_epilog(sbuf, *scount, c_stype, rbuf, *rcount, c_rtype, c_comm, r);
}
#endif
