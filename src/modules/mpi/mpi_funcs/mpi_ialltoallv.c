/* -*- c-file-style: "GNU" -*- */
/*
 * Copyright © CNRS, INRIA, Université Bordeaux 1, Telecom SudParis
 * See COPYING in top-level directory.
 */

#define _REENTRANT

#include <unistd.h>
#include <stdint.h>
#include <stdio.h>
#include <stdlib.h>
#include <sys/time.h>
#include <sys/timeb.h>
#include <dlfcn.h>
#include <string.h>

#include "mpi.h"
#include "mpi_eztrace.h"
#include "mpi_ev_codes.h"
#include "eztrace.h"



#ifdef USE_MPI3
static void MPI_Ialltoallv_prolog (CONST void *sendbuf __attribute__((unused)),
				  CONST int *sendcnts __attribute__((unused)),
				  CONST int *sdispls __attribute__((unused)),
				  MPI_Datatype sendtype,
				  void *recvbuf __attribute__((unused)),
				  CONST int *recvcnts __attribute__((unused)),
				   CONST int *rdispls __attribute__((unused)),
				   MPI_Datatype recvtype,
				   MPI_Comm comm,
				   MPI_Request *r)
{
  int rank = -1;
  int size = -1;
  libMPI_Comm_size(comm, &size);
  libMPI_Comm_rank(comm, &rank);

  int ssize, rsize;
  MPI_Type_size(sendtype, &ssize);
  MPI_Type_size(recvtype, &rsize);
  int data_size = sendcnts[0] * ssize;

  EZTRACE_EVENT_PACKED_5(EZTRACE_MPI_IALLTOALLV, (app_ptr)comm, size, rank, (app_ptr)r, data_size);
}

static int MPI_Ialltoallv_core (CONST void *sendbuf, CONST int *sendcnts, CONST int *sdispls, MPI_Datatype sendtype,
				void *recvbuf, CONST int *recvcnts, CONST int *rdispls, MPI_Datatype recvtype,
				MPI_Comm comm,
				MPI_Request *r)
{
  return libMPI_Ialltoallv (sendbuf, sendcnts, sdispls, sendtype, recvbuf, recvcnts, rdispls, recvtype, comm, r);
}

static void MPI_Ialltoallv_epilog (CONST void *sendbuf __attribute__((unused)),
				   CONST int *sendcnts __attribute__((unused)),
				   CONST int *sdispls __attribute__((unused)),
				   MPI_Datatype sendtype __attribute__((unused)),
				   void *recvbuf __attribute__((unused)),
				   CONST int *recvcnts __attribute__((unused)),
				   CONST int *rdispls __attribute__((unused)),
				   MPI_Datatype recvtype __attribute__((unused)),
				   MPI_Comm comm __attribute__((unused)),
				   MPI_Request *r) {
  EZTRACE_EVENT_PACKED_1(EZTRACE_MPI_STOP_IALLTOALLV, (app_ptr)r);
}


int MPI_Ialltoallv (CONST void *sendbuf, CONST int *sendcnts, CONST int *sdispls, MPI_Datatype sendtype,
		   void *recvbuf, CONST int *recvcnts, CONST int *rdispls, MPI_Datatype recvtype,
		    MPI_Comm comm, MPI_Request *r)
{
  FUNCTION_ENTRY;

  MPI_Ialltoallv_prolog (sendbuf, sendcnts, sdispls, sendtype, recvbuf, recvcnts, rdispls, recvtype, comm, r);
  int ret = MPI_Ialltoallv_core (sendbuf, sendcnts, sdispls, sendtype, recvbuf, recvcnts, rdispls, recvtype, comm, r);
  MPI_Ialltoallv_epilog (sendbuf, sendcnts, sdispls, sendtype, recvbuf, recvcnts, rdispls, recvtype, comm, r);

  return ret;
}

void mpif_ialltoallv_(void *sbuf, int *scount, int *sdispls, MPI_Fint *sd,
		     void *rbuf, int *rcount, int *rdispls, MPI_Fint *rd,
		      MPI_Fint *c, MPI_Fint *r, int *error)
{
  FUNCTION_ENTRY;
  MPI_Datatype c_stype = MPI_Type_f2c(*sd);
  MPI_Datatype c_rtype = MPI_Type_f2c(*rd);
  MPI_Comm c_comm = MPI_Comm_f2c(*c);
  MPI_Request c_req = MPI_Request_f2c(*r);

  MPI_Ialltoallv_prolog(sbuf, scount, sdispls, c_stype, rbuf, rcount, rdispls, c_rtype, c_comm, r);
  *error = MPI_Ialltoallv_core(sbuf, scount, sdispls, c_stype, rbuf, rcount, rdispls, c_rtype, c_comm, &c_req);
  *r = MPI_Request_c2f(c_req);
  MPI_Ialltoallv_epilog(sbuf, scount, sdispls, c_stype, rbuf, rcount, rdispls, c_rtype, c_comm, r);
}
#endif
