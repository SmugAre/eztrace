/*
 * Copyright (C) CNRS, INRIA, Université Bordeaux 1, Télécom SudParis
 * See COPYING in top-level directory.
 */

#include <stdio.h>
#include <GTG.h>
#include "eztrace_convert.h"
#include "eztrace_convert_macros.h"

#include "starpu_ev_codes.h"

//#define FUNC_NAME printf("%s\n", __FUNCTION__);

/* This structure contains information about the source of the starpu link */
struct starpu_link_source {
  char* source; /* The thread which submitted the task */
  varPrec time; /* date at which the task is submitted */
  struct starpu_task* task; /* Address of the task being submitted */
};

/* This list contains information about the source of the starpu link */
struct ezt_list_t starpu_link_source_list;

/* This structure contains the duration of a particular task */
struct starpu_task_duration {
  double start_time;
  struct starpu_task* task; /* Address of the task being executed */
};

/* This structure contains the durations of tasks of the same type */
struct starpu_task_type_stats {
  double min_time;
  double max_time;
  double ave_time;
  int nb_tasks;
  void* task_type;

  struct ezt_list_t starpu_task_duration_list; /* This list contains duration statistics for a particular task type */
};

/* This list contains duration statistics for a particular task type */
struct ezt_list_t starpu_task_type_stats_list;

char *jobName = NULL;

#define GET_PARAM_RAW_1(p_ev, arg1) do {		    \
    litl_read_get_param_1(p_ev, arg1);			    \
  } while(0)


typedef struct s_nameList {
    char *name;
    struct s_nameList *next;
} nameList;

nameList *nameL = NULL;
nameList *savePtr;
nameList *nameExec = NULL;
nameList *execPtr;



static struct ezt_list_token_t* __find_matching_link_source(struct starpu_task * task)
{
  struct ezt_list_token_t *t = NULL;
  struct starpu_link_source *r;
  ezt_list_foreach(&starpu_link_source_list, t) {
    r = (struct starpu_link_source *) t->data;
    if(r->task == task)
    return t;
  }
  return t;
}

static struct ezt_list_token_t* __find_matching_task(app_ptr task, struct ezt_list_t *starpu_task_duration_list)
{
  struct ezt_list_token_t *t = NULL;
  struct starpu_task_duration *r;
  ezt_list_foreach(starpu_task_duration_list, t) {
    r = (struct starpu_task_duration *) t->data;
    if((app_ptr)r->task == task)
    return t;
  }
  return t;
}

static struct ezt_list_token_t* __find_matching_task_type(app_ptr task_type)
{
  struct ezt_list_token_t *t = NULL;
  struct starpu_task_type_stats *r;
  ezt_list_foreach(&starpu_task_type_stats_list, t) {
    r = (struct starpu_task_type_stats *) t->data;
    if((app_ptr)r->task_type == task_type)
    return t;
  }
  return t;
}


int eztrace_convert_starpu_init();

int handle_starpu_events(eztrace_event_t *ev);

// Internals
void handleEZTRACE_starpu__starpu_driver_start_job();
void handleEZTRACE_starpu__starpu_driver_end_job() ;


// API
void handleEZTRACE_starpu_starpu_cublas_init_41() ;
void handleEZTRACE_starpu_starpu_cublas_init_42() ;
void handleEZTRACE_starpu_starpu_cublas_shutdown_43() ;
void handleEZTRACE_starpu_starpu_cublas_shutdown_44() ;
void handleEZTRACE_starpu_starpu_init_47() ;
void handleEZTRACE_starpu_starpu_init_48() ;
void handleEZTRACE_starpu_starpu_pause_49() ;
void handleEZTRACE_starpu_starpu_pause_50() ;
void handleEZTRACE_starpu_starpu_resume_51() ;
void handleEZTRACE_starpu_starpu_resume_52() ;
void handleEZTRACE_starpu_starpu_shutdown_53() ;
void handleEZTRACE_starpu_starpu_shutdown_54() ;
void handleEZTRACE_starpu_starpu_data_unregister_79() ;
void handleEZTRACE_starpu_starpu_data_unregister_80() ;
void handleEZTRACE_starpu_starpu_data_unregister_no_coherency_81() ;
void handleEZTRACE_starpu_starpu_data_unregister_no_coherency_82() ;
void handleEZTRACE_starpu_starpu_data_unregister_submit_83() ;
void handleEZTRACE_starpu_starpu_data_unregister_submit_84() ;
void handleEZTRACE_starpu_starpu_data_invalidate_85() ;
void handleEZTRACE_starpu_starpu_data_invalidate_86() ;
void handleEZTRACE_starpu_starpu_data_invalidate_submit_87() ;
void handleEZTRACE_starpu_starpu_data_invalidate_submit_88() ;
void handleEZTRACE_starpu_starpu_data_acquire_91() ;
void handleEZTRACE_starpu_starpu_data_acquire_92() ;
void handleEZTRACE_starpu_starpu_data_acquire_on_node_93() ;
void handleEZTRACE_starpu_starpu_data_acquire_on_node_94() ;
void handleEZTRACE_starpu_starpu_data_acquire_cb_95() ;
void handleEZTRACE_starpu_starpu_data_acquire_cb_96() ;
void handleEZTRACE_starpu_starpu_data_acquire_on_node_cb_97() ;
void handleEZTRACE_starpu_starpu_data_acquire_on_node_cb_98() ;
void handleEZTRACE_starpu_starpu_data_release_99() ;
void handleEZTRACE_starpu_starpu_data_release_100() ;
void handleEZTRACE_starpu_starpu_data_release_on_node_101() ;
void handleEZTRACE_starpu_starpu_data_release_on_node_102() ;
void handleEZTRACE_starpu_starpu_malloc_on_node_183() ;
void handleEZTRACE_starpu_starpu_malloc_on_node_184() ;
void handleEZTRACE_starpu_starpu_free_on_node_185() ;
void handleEZTRACE_starpu_starpu_free_on_node_186() ;
void handleEZTRACE_starpu_starpu_mpi_isend_311() ;
void handleEZTRACE_starpu_starpu_mpi_isend_312() ;
void handleEZTRACE_starpu_starpu_mpi_irecv_313() ;
void handleEZTRACE_starpu_starpu_mpi_irecv_314() ;
void handleEZTRACE_starpu_starpu_mpi_send_315() ;
void handleEZTRACE_starpu_starpu_mpi_send_316() ;
void handleEZTRACE_starpu_starpu_mpi_recv_317() ;
void handleEZTRACE_starpu_starpu_mpi_recv_318() ;
void handleEZTRACE_starpu_starpu_mpi_isend_detached_319() ;
void handleEZTRACE_starpu_starpu_mpi_isend_detached_320() ;
void handleEZTRACE_starpu_starpu_mpi_irecv_detached_321() ;
void handleEZTRACE_starpu_starpu_mpi_irecv_detached_322() ;
void handleEZTRACE_starpu_starpu_mpi_wait_323() ;
void handleEZTRACE_starpu_starpu_mpi_wait_324() ;
void handleEZTRACE_starpu_starpu_mpi_test_325() ;
void handleEZTRACE_starpu_starpu_mpi_test_326() ;
void handleEZTRACE_starpu_starpu_mpi_barrier_327() ;
void handleEZTRACE_starpu_starpu_mpi_barrier_328() ;
void handleEZTRACE_starpu_starpu_mpi_init_329() ;
void handleEZTRACE_starpu_starpu_mpi_init_330() ;
void handleEZTRACE_starpu_starpu_mpi_initialize_331() ;
void handleEZTRACE_starpu_starpu_mpi_initialize_332() ;
void handleEZTRACE_starpu_starpu_mpi_initialize_extended_333() ;
void handleEZTRACE_starpu_starpu_mpi_initialize_extended_334() ;
void handleEZTRACE_starpu_starpu_mpi_shutdown_335() ;
void handleEZTRACE_starpu_starpu_mpi_shutdown_336() ;
void handleEZTRACE_starpu_starpu_mpi_get_data_on_node_337() ;
void handleEZTRACE_starpu_starpu_mpi_get_data_on_node_338() ;
void handleEZTRACE_starpu_starpu_mpi_get_data_on_node_detached_339() ;
void handleEZTRACE_starpu_starpu_mpi_get_data_on_node_detached_340() ;
void handleEZTRACE_starpu_starpu_mpi_redux_data_341() ;
void handleEZTRACE_starpu_starpu_mpi_redux_data_342() ;
void handleEZTRACE_starpu_starpu_mpi_scatter_detached_343() ;
void handleEZTRACE_starpu_starpu_mpi_scatter_detached_344() ;
void handleEZTRACE_starpu_starpu_mpi_gather_detached_345() ;
void handleEZTRACE_starpu_starpu_mpi_gather_detached_346() ;
void handleEZTRACE_starpu_starpu_mpi_isend_detached_unlock_tag_347() ;
void handleEZTRACE_starpu_starpu_mpi_isend_detached_unlock_tag_348() ;
void handleEZTRACE_starpu_starpu_mpi_irecv_detached_unlock_tag_349() ;
void handleEZTRACE_starpu_starpu_mpi_irecv_detached_unlock_tag_350() ;
void handleEZTRACE_starpu_starpu_mpi_isend_array_detached_unlock_tag_351() ;
void handleEZTRACE_starpu_starpu_mpi_isend_array_detached_unlock_tag_352() ;
void handleEZTRACE_starpu_starpu_mpi_irecv_array_detached_unlock_tag_353() ;
void handleEZTRACE_starpu_starpu_mpi_irecv_array_detached_unlock_tag_354() ;
void handleEZTRACE_starpu_starpu_mpi_comm_amounts_retrieve_355() ;
void handleEZTRACE_starpu_starpu_mpi_comm_amounts_retrieve_356() ;
void handleEZTRACE_starpu_starpu_mpi_cache_flush_357() ;
void handleEZTRACE_starpu_starpu_mpi_cache_flush_358() ;
void handleEZTRACE_starpu_starpu_mpi_cache_flush_all_data_359() ;
void handleEZTRACE_starpu_starpu_mpi_cache_flush_all_data_360() ;
void handleEZTRACE_starpu_starpu_mpi_data_register_361() ;
void handleEZTRACE_starpu_starpu_mpi_data_register_362() ;
void handleEZTRACE_starpu_starpu_sched_ctx_add_workers_411() ;
void handleEZTRACE_starpu_starpu_sched_ctx_add_workers_412() ;
void handleEZTRACE_starpu_starpu_sched_ctx_remove_workers_413() ;
void handleEZTRACE_starpu_starpu_sched_ctx_remove_workers_414() ;
void handleEZTRACE_starpu_starpu_sched_ctx_delete_415() ;
void handleEZTRACE_starpu_starpu_sched_ctx_delete_416() ;
void handleEZTRACE_starpu_starpu_sched_ctx_set_inheritor_417() ;
void handleEZTRACE_starpu_starpu_sched_ctx_set_inheritor_418() ;
void handleEZTRACE_starpu_starpu_sched_ctx_set_context_419() ;
void handleEZTRACE_starpu_starpu_sched_ctx_set_context_420() ;
void handleEZTRACE_starpu_starpu_sched_ctx_get_context_421() ;
void handleEZTRACE_starpu_starpu_sched_ctx_get_context_422() ;
void handleEZTRACE_starpu_starpu_sched_ctx_stop_task_submission_423() ;
void handleEZTRACE_starpu_starpu_sched_ctx_stop_task_submission_424() ;
void handleEZTRACE_starpu_starpu_sched_ctx_finished_submit_425() ;
void handleEZTRACE_starpu_starpu_sched_ctx_finished_submit_426() ;
void handleEZTRACE_starpu_starpu_sched_ctx_create_worker_collection_461() ;
void handleEZTRACE_starpu_starpu_sched_ctx_create_worker_collection_462() ;
void handleEZTRACE_starpu_starpu_sched_ctx_delete_worker_collection_463() ;
void handleEZTRACE_starpu_starpu_sched_ctx_delete_worker_collection_464() ;
void handleEZTRACE_starpu_starpu_sched_ctx_get_worker_collection_465() ;
void handleEZTRACE_starpu_starpu_sched_ctx_get_worker_collection_466() ;
void handleEZTRACE_starpu_starpu_prefetch_task_input_on_node_495() ;
void handleEZTRACE_starpu_starpu_prefetch_task_input_on_node_496() ;
void handleEZTRACE_starpu_starpu_malloc_521() ;
void handleEZTRACE_starpu_starpu_malloc_522() ;
void handleEZTRACE_starpu_starpu_free_523() ;
void handleEZTRACE_starpu_starpu_free_524() ;
void handleEZTRACE_starpu_starpu_malloc_flags_525() ;
void handleEZTRACE_starpu_starpu_malloc_flags_526() ;
void handleEZTRACE_starpu_starpu_free_flags_527() ;
void handleEZTRACE_starpu_starpu_free_flags_528() ;
void handleEZTRACE_starpu_starpu_tag_wait_537() ;
void handleEZTRACE_starpu_starpu_tag_wait_538() ;
void handleEZTRACE_starpu_starpu_tag_wait_array_539() ;
void handleEZTRACE_starpu_starpu_tag_wait_array_540() ;
void handleEZTRACE_starpu_starpu_tag_notify_from_apps_541() ;
void handleEZTRACE_starpu_starpu_tag_notify_from_apps_542() ;
void handleEZTRACE_starpu_starpu_task_init_547() ;
void handleEZTRACE_starpu_starpu_task_init_548() ;
void handleEZTRACE_starpu_starpu_task_clean_549() ;
void handleEZTRACE_starpu_starpu_task_clean_550() ;
void handleEZTRACE_starpu_starpu_task_create_551() ;
void handleEZTRACE_starpu_starpu_task_create_552() ;
void handleEZTRACE_starpu_starpu_task_destroy_553() ;
void handleEZTRACE_starpu_starpu_task_destroy_554() ;
void handleEZTRACE_starpu_starpu_task_submit_555() ;
void handleEZTRACE_starpu_starpu_task_submit_556() ;
void handleEZTRACE_starpu_starpu_task_submit_to_ctx_557() ;
void handleEZTRACE_starpu_starpu_task_submit_to_ctx_558() ;
void handleEZTRACE_starpu_starpu_task_wait_559() ;
void handleEZTRACE_starpu_starpu_task_wait_560() ;
void handleEZTRACE_starpu_starpu_task_wait_for_all_561() ;
void handleEZTRACE_starpu_starpu_task_wait_for_all_562() ;
void handleEZTRACE_starpu_starpu_task_wait_for_all_in_ctx_563() ;
void handleEZTRACE_starpu_starpu_task_wait_for_all_in_ctx_564() ;
void handleEZTRACE_starpu_starpu_task_wait_for_no_ready_565() ;
void handleEZTRACE_starpu_starpu_task_wait_for_no_ready_566() ;
void handleEZTRACE_starpu_starpu_task_nready_567() ;
void handleEZTRACE_starpu_starpu_task_nready_568() ;
void handleEZTRACE_starpu_starpu_task_nsubmitted_569() ;
void handleEZTRACE_starpu_starpu_task_nsubmitted_570() ;
void handleEZTRACE_starpu_starpu_task_dup_581() ;
void handleEZTRACE_starpu_starpu_task_dup_582() ;
void handleEZTRACE_starpu_starpu_task_bundle_create_587() ;
void handleEZTRACE_starpu_starpu_task_bundle_create_588() ;
void handleEZTRACE_starpu_starpu_task_bundle_insert_589() ;
void handleEZTRACE_starpu_starpu_task_bundle_insert_590() ;
void handleEZTRACE_starpu_starpu_task_bundle_remove_591() ;
void handleEZTRACE_starpu_starpu_task_bundle_remove_592() ;
void handleEZTRACE_starpu_starpu_task_bundle_close_593() ;
void handleEZTRACE_starpu_starpu_task_bundle_close_594() ;
void handleEZTRACE_starpu_starpu_create_sync_task_595() ;
void handleEZTRACE_starpu_starpu_create_sync_task_596() ;
void handleEZTRACE_starpu_starpu_execute_on_each_worker_691() ;
void handleEZTRACE_starpu_starpu_execute_on_each_worker_692() ;
void handleEZTRACE_starpu_starpu_execute_on_each_worker_ex_693() ;
void handleEZTRACE_starpu_starpu_execute_on_each_worker_ex_694() ;
void handleEZTRACE_starpu_starpu_execute_on_specific_workers_695() ;
void handleEZTRACE_starpu_starpu_execute_on_specific_workers_696() ;
void handleEZTRACE_starpu_starpu_data_cpy_697() ;
void handleEZTRACE_starpu_starpu_data_cpy_698() ;

/* Constructor of the plugin.
 * This function registers the current module to eztrace_convert
 */
struct eztrace_convert_module starpu_module;
void libinit(void) __attribute__ ((constructor));
void libinit(void)
{
  starpu_module.api_version = EZTRACE_API_VERSION;

  /* Specify the initialization function.
   * This function will be called once all the plugins are loaded
   * and the trace is started.
   * This function usually declared StateTypes, LinkTypes, etc.
   */
  starpu_module.init = eztrace_convert_starpu_init;

  /* Specify the function to call for handling an event
   */
  starpu_module.handle = handle_starpu_events;

  /* Specify the module prefix */
  starpu_module.module_prefix = starpu_EVENTS_ID;

  asprintf(&starpu_module.name, "starpu");
  asprintf(&starpu_module.description, "module for StarPU framework");

  starpu_module.token.data = &starpu_module;

  /* Register the module to eztrace_convert */
  eztrace_convert_register_module(&starpu_module);

  //printf("module starpu loaded\n");
}

void libfinalize(void) __attribute__ ((destructor));
void libfinalize(void)
{
  //printf("unloading module starpu\n");
}



/*
 * This function will be called once all the plugins are loaded
 * and the trace is started.
 * This function usually declared StateTypes, LinkTypes, etc.
 */
int eztrace_convert_starpu_init()
{
    if (get_mode() == EZTRACE_CONVERT) {
	addContType("CT_Scheduler", "CT_Program", "Scheduler");
//	addContainer(0.00000, "CT_Sched", "CT_Scheduler", "C_Prog", "Scheduler", "0");
	addVarType("starpu_VAR__0", "submitted task", "CT_Process");

	// Internals
//	addEntityValue("starpu_STATE_0", "ST_Thread", "Executing a task", GTG_GREEN);

	// API
	addEntityValue("starpu_STATE_20", "ST_Thread", "starpu_cublas_init", GTG_DARKGREY);
	addEntityValue("starpu_STATE_21", "ST_Thread", "starpu_cublas_shutdown", GTG_DARKGREY);
	addEntityValue("starpu_STATE_23", "ST_Thread", "starpu_init", GTG_DARKGREY);
	addEntityValue("starpu_STATE_24", "ST_Thread", "starpu_pause", GTG_DARKGREY);
	addEntityValue("starpu_STATE_25", "ST_Thread", "starpu_resume", GTG_DARKGREY);
	addEntityValue("starpu_STATE_26", "ST_Thread", "starpu_shutdown", GTG_DARKGREY);
	addEntityValue("starpu_STATE_39", "ST_Thread", "starpu_data_unregister", GTG_DARKGREY);
	addEntityValue("starpu_STATE_40", "ST_Thread", "starpu_data_unregister_no_coherency", GTG_DARKGREY);
	addEntityValue("starpu_STATE_41", "ST_Thread", "starpu_data_unregister_submit", GTG_DARKGREY);
	addEntityValue("starpu_STATE_42", "ST_Thread", "starpu_data_invalidate", GTG_DARKGREY);
	addEntityValue("starpu_STATE_43", "ST_Thread", "starpu_data_invalidate_submit", GTG_DARKGREY);
	addEntityValue("starpu_STATE_45", "ST_Thread", "starpu_data_acquire", GTG_DARKGREY);
	addEntityValue("starpu_STATE_46", "ST_Thread", "starpu_data_acquire_on_node", GTG_DARKGREY);
	addEntityValue("starpu_STATE_47", "ST_Thread", "starpu_data_acquire_cb", GTG_DARKGREY);
	addEntityValue("starpu_STATE_48", "ST_Thread", "starpu_data_acquire_on_node_cb", GTG_DARKGREY);
	addEntityValue("starpu_STATE_49", "ST_Thread", "starpu_data_release", GTG_DARKGREY);
	addEntityValue("starpu_STATE_50", "ST_Thread", "starpu_data_release_on_node", GTG_DARKGREY);
	addEntityValue("starpu_STATE_52", "ST_Thread", "starpu_data_request_allocation", GTG_DARKGREY);
	addEntityValue("starpu_STATE_91", "ST_Thread", "starpu_malloc_on_node", GTG_DARKGREY);
	addEntityValue("starpu_STATE_92", "ST_Thread", "starpu_free_on_node", GTG_DARKGREY);
	addEntityValue("starpu_STATE_155", "ST_Thread", "starpu_mpi_isend", GTG_DARKGREY);
	addEntityValue("starpu_STATE_156", "ST_Thread", "starpu_mpi_irecv", GTG_DARKGREY);
	addEntityValue("starpu_STATE_157", "ST_Thread", "starpu_mpi_send", GTG_DARKGREY);
	addEntityValue("starpu_STATE_158", "ST_Thread", "starpu_mpi_recv", GTG_DARKGREY);
	addEntityValue("starpu_STATE_159", "ST_Thread", "starpu_mpi_isend_detached", GTG_DARKGREY);
	addEntityValue("starpu_STATE_160", "ST_Thread", "starpu_mpi_irecv_detached", GTG_DARKGREY);
	addEntityValue("starpu_STATE_161", "ST_Thread", "starpu_mpi_wait", GTG_DARKGREY);
	addEntityValue("starpu_STATE_162", "ST_Thread", "starpu_mpi_test", GTG_DARKGREY);
	addEntityValue("starpu_STATE_163", "ST_Thread", "starpu_mpi_barrier", GTG_DARKGREY);
	addEntityValue("starpu_STATE_164", "ST_Thread", "starpu_mpi_init", GTG_DARKGREY);
	addEntityValue("starpu_STATE_165", "ST_Thread", "starpu_mpi_initialize", GTG_DARKGREY);
	addEntityValue("starpu_STATE_166", "ST_Thread", "starpu_mpi_initialize_extended", GTG_DARKGREY);
	addEntityValue("starpu_STATE_167", "ST_Thread", "starpu_mpi_shutdown", GTG_DARKGREY);
	addEntityValue("starpu_STATE_168", "ST_Thread", "starpu_mpi_get_data_on_node", GTG_DARKGREY);
	addEntityValue("starpu_STATE_169", "ST_Thread", "starpu_mpi_get_data_on_node_detached", GTG_DARKGREY);
	addEntityValue("starpu_STATE_170", "ST_Thread", "starpu_mpi_redux_data", GTG_DARKGREY);
	addEntityValue("starpu_STATE_171", "ST_Thread", "starpu_mpi_scatter_detached", GTG_DARKGREY);
	addEntityValue("starpu_STATE_172", "ST_Thread", "starpu_mpi_gather_detached", GTG_DARKGREY);
	addEntityValue("starpu_STATE_173", "ST_Thread", "starpu_mpi_isend_detached_unlock_tag", GTG_DARKGREY);
	addEntityValue("starpu_STATE_174", "ST_Thread", "starpu_mpi_irecv_detached_unlock_tag", GTG_DARKGREY);
	addEntityValue("starpu_STATE_175", "ST_Thread", "starpu_mpi_isend_array_detached_unlock_tag", GTG_DARKGREY);
	addEntityValue("starpu_STATE_176", "ST_Thread", "starpu_mpi_irecv_array_detached_unlock_tag", GTG_DARKGREY);
	addEntityValue("starpu_STATE_177", "ST_Thread", "starpu_mpi_comm_amounts_retrieve", GTG_DARKGREY);
	addEntityValue("starpu_STATE_178", "ST_Thread", "starpu_mpi_cache_flush", GTG_DARKGREY);
	addEntityValue("starpu_STATE_179", "ST_Thread", "starpu_mpi_cache_flush_all_data", GTG_DARKGREY);
	addEntityValue("starpu_STATE_180", "ST_Thread", "starpu_mpi_data_register", GTG_DARKGREY);
	addEntityValue("starpu_STATE_205", "ST_Thread", "starpu_sched_ctx_add_workers", GTG_DARKGREY);
	addEntityValue("starpu_STATE_206", "ST_Thread", "starpu_sched_ctx_remove_workers", GTG_DARKGREY);
	addEntityValue("starpu_STATE_207", "ST_Thread", "starpu_sched_ctx_delete", GTG_DARKGREY);
	addEntityValue("starpu_STATE_208", "ST_Thread", "starpu_sched_ctx_set_inheritor", GTG_DARKGREY);
	addEntityValue("starpu_STATE_209", "ST_Thread", "starpu_sched_ctx_set_context", GTG_DARKGREY);
	addEntityValue("starpu_STATE_210", "ST_Thread", "starpu_sched_ctx_get_context", GTG_DARKGREY);
	addEntityValue("starpu_STATE_211", "ST_Thread", "starpu_sched_ctx_stop_task_submission", GTG_DARKGREY);
	addEntityValue("starpu_STATE_212", "ST_Thread", "starpu_sched_ctx_finished_submit", GTG_DARKGREY);
	addEntityValue("starpu_STATE_230", "ST_Thread", "starpu_sched_ctx_create_worker_collection", GTG_DARKGREY);
	addEntityValue("starpu_STATE_231", "ST_Thread", "starpu_sched_ctx_delete_worker_collection", GTG_DARKGREY);
	addEntityValue("starpu_STATE_232", "ST_Thread", "starpu_sched_ctx_get_worker_collection", GTG_DARKGREY);
	addEntityValue("starpu_STATE_247", "ST_Thread", "starpu_prefetch_task_input_on_node", GTG_DARKGREY);
	addEntityValue("starpu_STATE_260", "ST_Thread", "starpu_malloc", GTG_DARKGREY);
	addEntityValue("starpu_STATE_261", "ST_Thread", "starpu_free", GTG_DARKGREY);
	addEntityValue("starpu_STATE_262", "ST_Thread", "starpu_malloc_flags", GTG_DARKGREY);
	addEntityValue("starpu_STATE_263", "ST_Thread", "starpu_free_flags", GTG_DARKGREY);
	addEntityValue("starpu_STATE_268", "ST_Thread", "starpu_tag_wait", GTG_DARKGREY);
	addEntityValue("starpu_STATE_269", "ST_Thread", "starpu_tag_wait_array", GTG_DARKGREY);
	addEntityValue("starpu_STATE_270", "ST_Thread", "starpu_tag_notify_from_apps", GTG_DARKGREY);
	addEntityValue("starpu_STATE_273", "ST_Thread", "starpu_task_init", GTG_DARKGREY);
	addEntityValue("starpu_STATE_274", "ST_Thread", "starpu_task_clean", GTG_DARKGREY);
	addEntityValue("starpu_STATE_275", "ST_Thread", "starpu_task_create", GTG_DARKGREY);
	addEntityValue("starpu_STATE_276", "ST_Thread", "starpu_task_destroy", GTG_DARKGREY);
	addEntityValue("starpu_STATE_277", "ST_Thread", "starpu_task_submit", GTG_DARKGREY);
	addEntityValue("starpu_STATE_278", "ST_Thread", "starpu_task_submit_to_ctx", GTG_DARKGREY);
	addEntityValue("starpu_STATE_279", "ST_Thread", "starpu_task_wait", GTG_DARKGREY);
	addEntityValue("starpu_STATE_280", "ST_Thread", "starpu_task_wait_for_all", GTG_DARKGREY);
	addEntityValue("starpu_STATE_281", "ST_Thread", "starpu_task_wait_for_all_in_ctx", GTG_DARKGREY);
	addEntityValue("starpu_STATE_282", "ST_Thread", "starpu_task_wait_for_no_ready", GTG_DARKGREY);
	addEntityValue("starpu_STATE_283", "ST_Thread", "starpu_task_nready", GTG_DARKGREY);
	addEntityValue("starpu_STATE_284", "ST_Thread", "starpu_task_nsubmitted", GTG_DARKGREY);
	addEntityValue("starpu_STATE_290", "ST_Thread", "starpu_task_dup", GTG_DARKGREY);
	addEntityValue("starpu_STATE_293", "ST_Thread", "starpu_task_bundle_create", GTG_DARKGREY);
	addEntityValue("starpu_STATE_294", "ST_Thread", "starpu_task_bundle_insert", GTG_DARKGREY);
	addEntityValue("starpu_STATE_295", "ST_Thread", "starpu_task_bundle_remove", GTG_DARKGREY);
	addEntityValue("starpu_STATE_296", "ST_Thread", "starpu_task_bundle_close", GTG_DARKGREY);
	addEntityValue("starpu_STATE_297", "ST_Thread", "starpu_create_sync_task", GTG_DARKGREY);
	addEntityValue("starpu_STATE_345", "ST_Thread", "starpu_execute_on_each_worker", GTG_DARKGREY);
	addEntityValue("starpu_STATE_346", "ST_Thread", "starpu_execute_on_each_worker_ex", GTG_DARKGREY);
	addEntityValue("starpu_STATE_347", "ST_Thread", "starpu_execute_on_specific_workers", GTG_DARKGREY);
	addEntityValue("starpu_STATE_348", "ST_Thread", "starpu_data_cpy", GTG_DARKGREY);
    }
    ezt_list_new(&starpu_link_source_list);
    ezt_list_new(&starpu_task_type_stats_list);
}


/* This function is called by eztrace_convert for each event to
 * handle.
 * It shall return 1 if the event was handled successfully or
 * 0 otherwise.
 */
int
handle_starpu_events(eztrace_event_t *ev)
{
  switch (LITL_READ_GET_CODE(ev)) {
  case EZTRACE_starpu__starpu_driver_start_job:
	handleEZTRACE_starpu__starpu_driver_start_job();
	break;
    case EZTRACE_starpu__starpu_driver_end_job:
	handleEZTRACE_starpu__starpu_driver_end_job();
	break;
  case EZTRACE_starpu_starpu_cublas_init_41:
	handleEZTRACE_starpu_starpu_cublas_init_41();
	break;
	case EZTRACE_starpu_starpu_cublas_init_42:
	handleEZTRACE_starpu_starpu_cublas_init_42();
	break;
	case EZTRACE_starpu_starpu_cublas_shutdown_43:
	handleEZTRACE_starpu_starpu_cublas_shutdown_43();
	break;
	case EZTRACE_starpu_starpu_cublas_shutdown_44:
	handleEZTRACE_starpu_starpu_cublas_shutdown_44();
	break;
	case EZTRACE_starpu_starpu_init_47:
	handleEZTRACE_starpu_starpu_init_47();
	break;
	case EZTRACE_starpu_starpu_init_48:
	handleEZTRACE_starpu_starpu_init_48();
	break;
	case EZTRACE_starpu_starpu_pause_49:
	handleEZTRACE_starpu_starpu_pause_49();
	break;
	case EZTRACE_starpu_starpu_pause_50:
	handleEZTRACE_starpu_starpu_pause_50();
	break;
	case EZTRACE_starpu_starpu_resume_51:
	handleEZTRACE_starpu_starpu_resume_51();
	break;
	case EZTRACE_starpu_starpu_resume_52:
	handleEZTRACE_starpu_starpu_resume_52();
	break;
	case EZTRACE_starpu_starpu_shutdown_53:
	handleEZTRACE_starpu_starpu_shutdown_53();
	break;
	case EZTRACE_starpu_starpu_shutdown_54:
	handleEZTRACE_starpu_starpu_shutdown_54();
	break;
	case EZTRACE_starpu_starpu_data_unregister_79:
	handleEZTRACE_starpu_starpu_data_unregister_79();
	break;
	case EZTRACE_starpu_starpu_data_unregister_80:
	handleEZTRACE_starpu_starpu_data_unregister_80();
	break;
	case EZTRACE_starpu_starpu_data_unregister_no_coherency_81:
	handleEZTRACE_starpu_starpu_data_unregister_no_coherency_81();
	break;
	case EZTRACE_starpu_starpu_data_unregister_no_coherency_82:
	handleEZTRACE_starpu_starpu_data_unregister_no_coherency_82();
	break;
	case EZTRACE_starpu_starpu_data_unregister_submit_83:
	handleEZTRACE_starpu_starpu_data_unregister_submit_83();
	break;
	case EZTRACE_starpu_starpu_data_unregister_submit_84:
	handleEZTRACE_starpu_starpu_data_unregister_submit_84();
	break;
	case EZTRACE_starpu_starpu_data_invalidate_85:
	handleEZTRACE_starpu_starpu_data_invalidate_85();
	break;
	case EZTRACE_starpu_starpu_data_invalidate_86:
	handleEZTRACE_starpu_starpu_data_invalidate_86();
	break;
	case EZTRACE_starpu_starpu_data_invalidate_submit_87:
	handleEZTRACE_starpu_starpu_data_invalidate_submit_87();
	break;
	case EZTRACE_starpu_starpu_data_invalidate_submit_88:
	handleEZTRACE_starpu_starpu_data_invalidate_submit_88();
	break;
	case EZTRACE_starpu_starpu_data_acquire_91:
	handleEZTRACE_starpu_starpu_data_acquire_91();
	break;
	case EZTRACE_starpu_starpu_data_acquire_92:
	handleEZTRACE_starpu_starpu_data_acquire_92();
	break;
	case EZTRACE_starpu_starpu_data_acquire_on_node_93:
	handleEZTRACE_starpu_starpu_data_acquire_on_node_93();
	break;
	case EZTRACE_starpu_starpu_data_acquire_on_node_94:
	handleEZTRACE_starpu_starpu_data_acquire_on_node_94();
	break;
	case EZTRACE_starpu_starpu_data_acquire_cb_95:
	handleEZTRACE_starpu_starpu_data_acquire_cb_95();
	break;
	case EZTRACE_starpu_starpu_data_acquire_cb_96:
	handleEZTRACE_starpu_starpu_data_acquire_cb_96();
	break;
	case EZTRACE_starpu_starpu_data_acquire_on_node_cb_97:
	handleEZTRACE_starpu_starpu_data_acquire_on_node_cb_97();
	break;
	case EZTRACE_starpu_starpu_data_acquire_on_node_cb_98:
	handleEZTRACE_starpu_starpu_data_acquire_on_node_cb_98();
	break;
	case EZTRACE_starpu_starpu_data_release_99:
	handleEZTRACE_starpu_starpu_data_release_99();
	break;
	case EZTRACE_starpu_starpu_data_release_100:
	handleEZTRACE_starpu_starpu_data_release_100();
	break;
	case EZTRACE_starpu_starpu_data_release_on_node_101:
	handleEZTRACE_starpu_starpu_data_release_on_node_101();
	break;
	case EZTRACE_starpu_starpu_data_release_on_node_102:
	handleEZTRACE_starpu_starpu_data_release_on_node_102();
	break;
	case EZTRACE_starpu_starpu_malloc_on_node_183:
	handleEZTRACE_starpu_starpu_malloc_on_node_183();
	break;
	case EZTRACE_starpu_starpu_malloc_on_node_184:
	handleEZTRACE_starpu_starpu_malloc_on_node_184();
	break;
	case EZTRACE_starpu_starpu_free_on_node_185:
	handleEZTRACE_starpu_starpu_free_on_node_185();
	break;
	case EZTRACE_starpu_starpu_free_on_node_186:
	handleEZTRACE_starpu_starpu_free_on_node_186();
	break;
	case EZTRACE_starpu_starpu_mpi_isend_311:
	handleEZTRACE_starpu_starpu_mpi_isend_311();
	break;
	case EZTRACE_starpu_starpu_mpi_isend_312:
	handleEZTRACE_starpu_starpu_mpi_isend_312();
	break;
	case EZTRACE_starpu_starpu_mpi_irecv_313:
	handleEZTRACE_starpu_starpu_mpi_irecv_313();
	break;
	case EZTRACE_starpu_starpu_mpi_irecv_314:
	handleEZTRACE_starpu_starpu_mpi_irecv_314();
	break;
	case EZTRACE_starpu_starpu_mpi_send_315:
	handleEZTRACE_starpu_starpu_mpi_send_315();
	break;
	case EZTRACE_starpu_starpu_mpi_send_316:
	handleEZTRACE_starpu_starpu_mpi_send_316();
	break;
	case EZTRACE_starpu_starpu_mpi_recv_317:
	handleEZTRACE_starpu_starpu_mpi_recv_317();
	break;
	case EZTRACE_starpu_starpu_mpi_recv_318:
	handleEZTRACE_starpu_starpu_mpi_recv_318();
	break;
	case EZTRACE_starpu_starpu_mpi_isend_detached_319:
	handleEZTRACE_starpu_starpu_mpi_isend_detached_319();
	break;
	case EZTRACE_starpu_starpu_mpi_isend_detached_320:
	handleEZTRACE_starpu_starpu_mpi_isend_detached_320();
	break;
	case EZTRACE_starpu_starpu_mpi_irecv_detached_321:
	handleEZTRACE_starpu_starpu_mpi_irecv_detached_321();
	break;
	case EZTRACE_starpu_starpu_mpi_irecv_detached_322:
	handleEZTRACE_starpu_starpu_mpi_irecv_detached_322();
	break;
	case EZTRACE_starpu_starpu_mpi_wait_323:
	handleEZTRACE_starpu_starpu_mpi_wait_323();
	break;
	case EZTRACE_starpu_starpu_mpi_wait_324:
	handleEZTRACE_starpu_starpu_mpi_wait_324();
	break;
	case EZTRACE_starpu_starpu_mpi_test_325:
	handleEZTRACE_starpu_starpu_mpi_test_325();
	break;
	case EZTRACE_starpu_starpu_mpi_test_326:
	handleEZTRACE_starpu_starpu_mpi_test_326();
	break;
	case EZTRACE_starpu_starpu_mpi_barrier_327:
	handleEZTRACE_starpu_starpu_mpi_barrier_327();
	break;
	case EZTRACE_starpu_starpu_mpi_barrier_328:
	handleEZTRACE_starpu_starpu_mpi_barrier_328();
	break;
	case EZTRACE_starpu_starpu_mpi_init_329:
	handleEZTRACE_starpu_starpu_mpi_init_329();
	break;
	case EZTRACE_starpu_starpu_mpi_init_330:
	handleEZTRACE_starpu_starpu_mpi_init_330();
	break;
	case EZTRACE_starpu_starpu_mpi_initialize_331:
	handleEZTRACE_starpu_starpu_mpi_initialize_331();
	break;
	case EZTRACE_starpu_starpu_mpi_initialize_332:
	handleEZTRACE_starpu_starpu_mpi_initialize_332();
	break;
	case EZTRACE_starpu_starpu_mpi_initialize_extended_333:
	handleEZTRACE_starpu_starpu_mpi_initialize_extended_333();
	break;
	case EZTRACE_starpu_starpu_mpi_initialize_extended_334:
	handleEZTRACE_starpu_starpu_mpi_initialize_extended_334();
	break;
	case EZTRACE_starpu_starpu_mpi_shutdown_335:
	handleEZTRACE_starpu_starpu_mpi_shutdown_335();
	break;
	case EZTRACE_starpu_starpu_mpi_shutdown_336:
	handleEZTRACE_starpu_starpu_mpi_shutdown_336();
	break;
	case EZTRACE_starpu_starpu_mpi_get_data_on_node_337:
	handleEZTRACE_starpu_starpu_mpi_get_data_on_node_337();
	break;
	case EZTRACE_starpu_starpu_mpi_get_data_on_node_338:
	handleEZTRACE_starpu_starpu_mpi_get_data_on_node_338();
	break;
	case EZTRACE_starpu_starpu_mpi_get_data_on_node_detached_339:
	handleEZTRACE_starpu_starpu_mpi_get_data_on_node_detached_339();
	break;
	case EZTRACE_starpu_starpu_mpi_get_data_on_node_detached_340:
	handleEZTRACE_starpu_starpu_mpi_get_data_on_node_detached_340();
	break;
	case EZTRACE_starpu_starpu_mpi_redux_data_341:
	handleEZTRACE_starpu_starpu_mpi_redux_data_341();
	break;
	case EZTRACE_starpu_starpu_mpi_redux_data_342:
	handleEZTRACE_starpu_starpu_mpi_redux_data_342();
	break;
	case EZTRACE_starpu_starpu_mpi_scatter_detached_343:
	handleEZTRACE_starpu_starpu_mpi_scatter_detached_343();
	break;
	case EZTRACE_starpu_starpu_mpi_scatter_detached_344:
	handleEZTRACE_starpu_starpu_mpi_scatter_detached_344();
	break;
	case EZTRACE_starpu_starpu_mpi_gather_detached_345:
	handleEZTRACE_starpu_starpu_mpi_gather_detached_345();
	break;
	case EZTRACE_starpu_starpu_mpi_gather_detached_346:
	handleEZTRACE_starpu_starpu_mpi_gather_detached_346();
	break;
	case EZTRACE_starpu_starpu_mpi_isend_detached_unlock_tag_347:
	handleEZTRACE_starpu_starpu_mpi_isend_detached_unlock_tag_347();
	break;
	case EZTRACE_starpu_starpu_mpi_isend_detached_unlock_tag_348:
	handleEZTRACE_starpu_starpu_mpi_isend_detached_unlock_tag_348();
	break;
	case EZTRACE_starpu_starpu_mpi_irecv_detached_unlock_tag_349:
	handleEZTRACE_starpu_starpu_mpi_irecv_detached_unlock_tag_349();
	break;
	case EZTRACE_starpu_starpu_mpi_irecv_detached_unlock_tag_350:
	handleEZTRACE_starpu_starpu_mpi_irecv_detached_unlock_tag_350();
	break;
	case EZTRACE_starpu_starpu_mpi_isend_array_detached_unlock_tag_351:
	handleEZTRACE_starpu_starpu_mpi_isend_array_detached_unlock_tag_351();
	break;
	case EZTRACE_starpu_starpu_mpi_isend_array_detached_unlock_tag_352:
	handleEZTRACE_starpu_starpu_mpi_isend_array_detached_unlock_tag_352();
	break;
	case EZTRACE_starpu_starpu_mpi_irecv_array_detached_unlock_tag_353:
	handleEZTRACE_starpu_starpu_mpi_irecv_array_detached_unlock_tag_353();
	break;
	case EZTRACE_starpu_starpu_mpi_irecv_array_detached_unlock_tag_354:
	handleEZTRACE_starpu_starpu_mpi_irecv_array_detached_unlock_tag_354();
	break;
	case EZTRACE_starpu_starpu_mpi_comm_amounts_retrieve_355:
	handleEZTRACE_starpu_starpu_mpi_comm_amounts_retrieve_355();
	break;
	case EZTRACE_starpu_starpu_mpi_comm_amounts_retrieve_356:
	handleEZTRACE_starpu_starpu_mpi_comm_amounts_retrieve_356();
	break;
	case EZTRACE_starpu_starpu_mpi_cache_flush_357:
	handleEZTRACE_starpu_starpu_mpi_cache_flush_357();
	break;
	case EZTRACE_starpu_starpu_mpi_cache_flush_358:
	handleEZTRACE_starpu_starpu_mpi_cache_flush_358();
	break;
	case EZTRACE_starpu_starpu_mpi_cache_flush_all_data_359:
	handleEZTRACE_starpu_starpu_mpi_cache_flush_all_data_359();
	break;
	case EZTRACE_starpu_starpu_mpi_cache_flush_all_data_360:
	handleEZTRACE_starpu_starpu_mpi_cache_flush_all_data_360();
	break;
	case EZTRACE_starpu_starpu_mpi_data_register_361:
	handleEZTRACE_starpu_starpu_mpi_data_register_361();
	break;
	case EZTRACE_starpu_starpu_mpi_data_register_362:
	handleEZTRACE_starpu_starpu_mpi_data_register_362();
	break;
	case EZTRACE_starpu_starpu_sched_ctx_add_workers_411:
	handleEZTRACE_starpu_starpu_sched_ctx_add_workers_411();
	break;
	case EZTRACE_starpu_starpu_sched_ctx_add_workers_412:
	handleEZTRACE_starpu_starpu_sched_ctx_add_workers_412();
	break;
	case EZTRACE_starpu_starpu_sched_ctx_remove_workers_413:
	handleEZTRACE_starpu_starpu_sched_ctx_remove_workers_413();
	break;
	case EZTRACE_starpu_starpu_sched_ctx_remove_workers_414:
	handleEZTRACE_starpu_starpu_sched_ctx_remove_workers_414();
	break;
	case EZTRACE_starpu_starpu_sched_ctx_delete_415:
	handleEZTRACE_starpu_starpu_sched_ctx_delete_415();
	break;
	case EZTRACE_starpu_starpu_sched_ctx_delete_416:
	handleEZTRACE_starpu_starpu_sched_ctx_delete_416();
	break;
	case EZTRACE_starpu_starpu_sched_ctx_set_inheritor_417:
	handleEZTRACE_starpu_starpu_sched_ctx_set_inheritor_417();
	break;
	case EZTRACE_starpu_starpu_sched_ctx_set_inheritor_418:
	handleEZTRACE_starpu_starpu_sched_ctx_set_inheritor_418();
	break;
	case EZTRACE_starpu_starpu_sched_ctx_set_context_419:
	handleEZTRACE_starpu_starpu_sched_ctx_set_context_419();
	break;
	case EZTRACE_starpu_starpu_sched_ctx_set_context_420:
	handleEZTRACE_starpu_starpu_sched_ctx_set_context_420();
	break;
	case EZTRACE_starpu_starpu_sched_ctx_get_context_421:
	handleEZTRACE_starpu_starpu_sched_ctx_get_context_421();
	break;
	case EZTRACE_starpu_starpu_sched_ctx_get_context_422:
	handleEZTRACE_starpu_starpu_sched_ctx_get_context_422();
	break;
	case EZTRACE_starpu_starpu_sched_ctx_stop_task_submission_423:
	handleEZTRACE_starpu_starpu_sched_ctx_stop_task_submission_423();
	break;
	case EZTRACE_starpu_starpu_sched_ctx_stop_task_submission_424:
	handleEZTRACE_starpu_starpu_sched_ctx_stop_task_submission_424();
	break;
	case EZTRACE_starpu_starpu_sched_ctx_finished_submit_425:
	handleEZTRACE_starpu_starpu_sched_ctx_finished_submit_425();
	break;
	case EZTRACE_starpu_starpu_sched_ctx_finished_submit_426:
	handleEZTRACE_starpu_starpu_sched_ctx_finished_submit_426();
	break;
	case EZTRACE_starpu_starpu_sched_ctx_create_worker_collection_461:
	handleEZTRACE_starpu_starpu_sched_ctx_create_worker_collection_461();
	break;
	case EZTRACE_starpu_starpu_sched_ctx_create_worker_collection_462:
	handleEZTRACE_starpu_starpu_sched_ctx_create_worker_collection_462();
	break;
	case EZTRACE_starpu_starpu_sched_ctx_delete_worker_collection_463:
	handleEZTRACE_starpu_starpu_sched_ctx_delete_worker_collection_463();
	break;
	case EZTRACE_starpu_starpu_sched_ctx_delete_worker_collection_464:
	handleEZTRACE_starpu_starpu_sched_ctx_delete_worker_collection_464();
	break;
	case EZTRACE_starpu_starpu_sched_ctx_get_worker_collection_465:
	handleEZTRACE_starpu_starpu_sched_ctx_get_worker_collection_465();
	break;
	case EZTRACE_starpu_starpu_sched_ctx_get_worker_collection_466:
	handleEZTRACE_starpu_starpu_sched_ctx_get_worker_collection_466();
	break;
	case EZTRACE_starpu_starpu_prefetch_task_input_on_node_495:
	handleEZTRACE_starpu_starpu_prefetch_task_input_on_node_495();
	break;
	case EZTRACE_starpu_starpu_prefetch_task_input_on_node_496:
	handleEZTRACE_starpu_starpu_prefetch_task_input_on_node_496();
	break;
	case EZTRACE_starpu_starpu_malloc_521:
	handleEZTRACE_starpu_starpu_malloc_521();
	break;
	case EZTRACE_starpu_starpu_malloc_522:
	handleEZTRACE_starpu_starpu_malloc_522();
	break;
	case EZTRACE_starpu_starpu_free_523:
	handleEZTRACE_starpu_starpu_free_523();
	break;
	case EZTRACE_starpu_starpu_free_524:
	handleEZTRACE_starpu_starpu_free_524();
	break;
	case EZTRACE_starpu_starpu_malloc_flags_525:
	handleEZTRACE_starpu_starpu_malloc_flags_525();
	break;
	case EZTRACE_starpu_starpu_malloc_flags_526:
	handleEZTRACE_starpu_starpu_malloc_flags_526();
	break;
	case EZTRACE_starpu_starpu_free_flags_527:
	handleEZTRACE_starpu_starpu_free_flags_527();
	break;
	case EZTRACE_starpu_starpu_free_flags_528:
	handleEZTRACE_starpu_starpu_free_flags_528();
	break;
	case EZTRACE_starpu_starpu_tag_wait_537:
	handleEZTRACE_starpu_starpu_tag_wait_537();
	break;
	case EZTRACE_starpu_starpu_tag_wait_538:
	handleEZTRACE_starpu_starpu_tag_wait_538();
	break;
	case EZTRACE_starpu_starpu_tag_wait_array_539:
	handleEZTRACE_starpu_starpu_tag_wait_array_539();
	break;
	case EZTRACE_starpu_starpu_tag_wait_array_540:
	handleEZTRACE_starpu_starpu_tag_wait_array_540();
	break;
	case EZTRACE_starpu_starpu_tag_notify_from_apps_541:
	handleEZTRACE_starpu_starpu_tag_notify_from_apps_541();
	break;
	case EZTRACE_starpu_starpu_tag_notify_from_apps_542:
	handleEZTRACE_starpu_starpu_tag_notify_from_apps_542();
	break;
	case EZTRACE_starpu_starpu_task_init_547:
	handleEZTRACE_starpu_starpu_task_init_547();
	break;
	case EZTRACE_starpu_starpu_task_init_548:
	handleEZTRACE_starpu_starpu_task_init_548();
	break;
	case EZTRACE_starpu_starpu_task_clean_549:
	handleEZTRACE_starpu_starpu_task_clean_549();
	break;
	case EZTRACE_starpu_starpu_task_clean_550:
	handleEZTRACE_starpu_starpu_task_clean_550();
	break;
	case EZTRACE_starpu_starpu_task_create_551:
	handleEZTRACE_starpu_starpu_task_create_551();
	break;
	case EZTRACE_starpu_starpu_task_create_552:
	handleEZTRACE_starpu_starpu_task_create_552();
	break;
	case EZTRACE_starpu_starpu_task_destroy_553:
	handleEZTRACE_starpu_starpu_task_destroy_553();
	break;
	case EZTRACE_starpu_starpu_task_destroy_554:
	handleEZTRACE_starpu_starpu_task_destroy_554();
	break;
	case EZTRACE_starpu_starpu_task_submit_555:
	handleEZTRACE_starpu_starpu_task_submit_555();
	break;
	case EZTRACE_starpu_starpu_task_submit_556:
	handleEZTRACE_starpu_starpu_task_submit_556();
	break;
	case EZTRACE_starpu_starpu_task_submit_to_ctx_557:
	handleEZTRACE_starpu_starpu_task_submit_to_ctx_557();
	break;
	case EZTRACE_starpu_starpu_task_submit_to_ctx_558:
	handleEZTRACE_starpu_starpu_task_submit_to_ctx_558();
	break;
	case EZTRACE_starpu_starpu_task_wait_559:
	handleEZTRACE_starpu_starpu_task_wait_559();
	break;
	case EZTRACE_starpu_starpu_task_wait_560:
	handleEZTRACE_starpu_starpu_task_wait_560();
	break;
	case EZTRACE_starpu_starpu_task_wait_for_all_561:
	handleEZTRACE_starpu_starpu_task_wait_for_all_561();
	break;
	case EZTRACE_starpu_starpu_task_wait_for_all_562:
	handleEZTRACE_starpu_starpu_task_wait_for_all_562();
	break;
	case EZTRACE_starpu_starpu_task_wait_for_all_in_ctx_563:
	handleEZTRACE_starpu_starpu_task_wait_for_all_in_ctx_563();
	break;
	case EZTRACE_starpu_starpu_task_wait_for_all_in_ctx_564:
	handleEZTRACE_starpu_starpu_task_wait_for_all_in_ctx_564();
	break;
	case EZTRACE_starpu_starpu_task_wait_for_no_ready_565:
	handleEZTRACE_starpu_starpu_task_wait_for_no_ready_565();
	break;
	case EZTRACE_starpu_starpu_task_wait_for_no_ready_566:
	handleEZTRACE_starpu_starpu_task_wait_for_no_ready_566();
	break;
	case EZTRACE_starpu_starpu_task_nready_567:
	handleEZTRACE_starpu_starpu_task_nready_567();
	break;
	case EZTRACE_starpu_starpu_task_nready_568:
	handleEZTRACE_starpu_starpu_task_nready_568();
	break;
	case EZTRACE_starpu_starpu_task_nsubmitted_569:
	handleEZTRACE_starpu_starpu_task_nsubmitted_569();
	break;
	case EZTRACE_starpu_starpu_task_nsubmitted_570:
	handleEZTRACE_starpu_starpu_task_nsubmitted_570();
	break;
	case EZTRACE_starpu_starpu_task_dup_581:
	handleEZTRACE_starpu_starpu_task_dup_581();
	break;
	case EZTRACE_starpu_starpu_task_dup_582:
	handleEZTRACE_starpu_starpu_task_dup_582();
	break;
	case EZTRACE_starpu_starpu_task_bundle_create_587:
	handleEZTRACE_starpu_starpu_task_bundle_create_587();
	break;
	case EZTRACE_starpu_starpu_task_bundle_create_588:
	handleEZTRACE_starpu_starpu_task_bundle_create_588();
	break;
	case EZTRACE_starpu_starpu_task_bundle_insert_589:
	handleEZTRACE_starpu_starpu_task_bundle_insert_589();
	break;
	case EZTRACE_starpu_starpu_task_bundle_insert_590:
	handleEZTRACE_starpu_starpu_task_bundle_insert_590();
	break;
	case EZTRACE_starpu_starpu_task_bundle_remove_591:
	handleEZTRACE_starpu_starpu_task_bundle_remove_591();
	break;
	case EZTRACE_starpu_starpu_task_bundle_remove_592:
	handleEZTRACE_starpu_starpu_task_bundle_remove_592();
	break;
	case EZTRACE_starpu_starpu_task_bundle_close_593:
	handleEZTRACE_starpu_starpu_task_bundle_close_593();
	break;
	case EZTRACE_starpu_starpu_task_bundle_close_594:
	handleEZTRACE_starpu_starpu_task_bundle_close_594();
	break;
	case EZTRACE_starpu_starpu_create_sync_task_595:
	handleEZTRACE_starpu_starpu_create_sync_task_595();
	break;
	case EZTRACE_starpu_starpu_create_sync_task_596:
	handleEZTRACE_starpu_starpu_create_sync_task_596();
	break;
	case EZTRACE_starpu_starpu_execute_on_each_worker_691:
	handleEZTRACE_starpu_starpu_execute_on_each_worker_691();
	break;
	case EZTRACE_starpu_starpu_execute_on_each_worker_692:
	handleEZTRACE_starpu_starpu_execute_on_each_worker_692();
	break;
	case EZTRACE_starpu_starpu_execute_on_each_worker_ex_693:
	handleEZTRACE_starpu_starpu_execute_on_each_worker_ex_693();
	break;
	case EZTRACE_starpu_starpu_execute_on_each_worker_ex_694:
	handleEZTRACE_starpu_starpu_execute_on_each_worker_ex_694();
	break;
	case EZTRACE_starpu_starpu_execute_on_specific_workers_695:
	handleEZTRACE_starpu_starpu_execute_on_specific_workers_695();
	break;
	case EZTRACE_starpu_starpu_execute_on_specific_workers_696:
	handleEZTRACE_starpu_starpu_execute_on_specific_workers_696();
	break;
	case EZTRACE_starpu_starpu_data_cpy_697:
	handleEZTRACE_starpu_starpu_data_cpy_697();
	break;
	case EZTRACE_starpu_starpu_data_cpy_698:
	handleEZTRACE_starpu_starpu_data_cpy_698();
	break;

    default:
      /* The event was not handled */
      return 0;
    }
  return 1;
}

// Internals
void handleEZTRACE_starpu__starpu_driver_start_job() {
    FUNC_NAME;
    DECLARE_THREAD_ID_STR(thread_id, CUR_INDEX, CUR_THREAD_ID);
    char *proc = calloc(1, sizeof(char) * 9);
    sprintf(proc, "CT_SP#%i", CUR_RANK);
    CHANGE() subVar (CURRENT, "starpu_VAR__0", proc, (varPrec) 1);
    // CHANGE() pushState (CURRENT, "ST_Thread", thread_id, "starpu_STATE_0");
    //   free(proc);
}

void handleEZTRACE_starpu__starpu_driver_end_job() {
	FUNC_NAME;
	DECLARE_THREAD_ID_STR(thread_id, CUR_INDEX, CUR_THREAD_ID);
//	CHANGE() popState (CURRENT, "ST_Thread", thread_id);
}


//API
void handleEZTRACE_starpu_starpu_cublas_init_41() {
    FUNC_NAME;
    DECLARE_THREAD_ID_STR(thread_id, CUR_INDEX, CUR_THREAD_ID);
    CHANGE() pushState (CURRENT, "ST_Thread", thread_id, "starpu_STATE_20");
}
void handleEZTRACE_starpu_starpu_cublas_init_42() {
    FUNC_NAME;
    DECLARE_THREAD_ID_STR(thread_id, CUR_INDEX, CUR_THREAD_ID);
    CHANGE() popState (CURRENT, "ST_Thread", thread_id);
}
void handleEZTRACE_starpu_starpu_cublas_shutdown_43() {
    FUNC_NAME;
    DECLARE_THREAD_ID_STR(thread_id, CUR_INDEX, CUR_THREAD_ID);
    CHANGE() pushState (CURRENT, "ST_Thread", thread_id, "starpu_STATE_21");
}
void handleEZTRACE_starpu_starpu_cublas_shutdown_44() {
    FUNC_NAME;
    DECLARE_THREAD_ID_STR(thread_id, CUR_INDEX, CUR_THREAD_ID);
    CHANGE() popState (CURRENT, "ST_Thread", thread_id);
}
void handleEZTRACE_starpu_starpu_init_47() {
    FUNC_NAME;
    char *proc = calloc(1, sizeof(char) * 5);
    char *procCpt = calloc(1, sizeof(char) * 9);
    DECLARE_THREAD_ID_STR(thread_id, CUR_INDEX, CUR_THREAD_ID);
    CHANGE() pushState (CURRENT, "ST_Thread", thread_id, "starpu_STATE_23");
    sprintf(procCpt,"CT_SP#%i", CUR_RANK);
    sprintf(proc,"P#%i", CUR_RANK);
    addContainer(0.00000, procCpt, "CT_Scheduler", proc, "Task queue ", "0");
    CHANGE() setVar (CURRENT, "starpu_VAR__0", procCpt, 0);
}
void handleEZTRACE_starpu_starpu_init_48() {
    FUNC_NAME;
    DECLARE_THREAD_ID_STR(thread_id, CUR_INDEX, CUR_THREAD_ID);
    CHANGE() popState (CURRENT, "ST_Thread", thread_id);
}
void handleEZTRACE_starpu_starpu_pause_49() {
    FUNC_NAME;
    DECLARE_THREAD_ID_STR(thread_id, CUR_INDEX, CUR_THREAD_ID);
    CHANGE() pushState (CURRENT, "ST_Thread", thread_id, "starpu_STATE_24");
}
void handleEZTRACE_starpu_starpu_pause_50() {
    FUNC_NAME;
    DECLARE_THREAD_ID_STR(thread_id, CUR_INDEX, CUR_THREAD_ID);
    CHANGE() popState (CURRENT, "ST_Thread", thread_id);
}
void handleEZTRACE_starpu_starpu_resume_51() {
    FUNC_NAME;
    DECLARE_THREAD_ID_STR(thread_id, CUR_INDEX, CUR_THREAD_ID);
    CHANGE() pushState (CURRENT, "ST_Thread", thread_id, "starpu_STATE_25");
}
void handleEZTRACE_starpu_starpu_resume_52() {
    FUNC_NAME;
    DECLARE_THREAD_ID_STR(thread_id, CUR_INDEX, CUR_THREAD_ID);
    CHANGE() popState (CURRENT, "ST_Thread", thread_id);
}
void handleEZTRACE_starpu_starpu_shutdown_53() {
    FUNC_NAME;
    DECLARE_THREAD_ID_STR(thread_id, CUR_INDEX, CUR_THREAD_ID);
    CHANGE() pushState (CURRENT, "ST_Thread", thread_id, "starpu_STATE_26");
}
void handleEZTRACE_starpu_starpu_shutdown_54() {
    FUNC_NAME;
    DECLARE_THREAD_ID_STR(thread_id, CUR_INDEX, CUR_THREAD_ID);
    CHANGE() popState (CURRENT, "ST_Thread", thread_id);
}
void handleEZTRACE_starpu_starpu_data_unregister_79() {
    FUNC_NAME;
    DECLARE_THREAD_ID_STR(thread_id, CUR_INDEX, CUR_THREAD_ID);
    CHANGE() pushState (CURRENT, "ST_Thread", thread_id, "starpu_STATE_39");
}
void handleEZTRACE_starpu_starpu_data_unregister_80() {
    FUNC_NAME;
    DECLARE_THREAD_ID_STR(thread_id, CUR_INDEX, CUR_THREAD_ID);
    CHANGE() popState (CURRENT, "ST_Thread", thread_id);
}
void handleEZTRACE_starpu_starpu_data_unregister_no_coherency_81() {
    FUNC_NAME;
    DECLARE_THREAD_ID_STR(thread_id, CUR_INDEX, CUR_THREAD_ID);
    CHANGE() pushState (CURRENT, "ST_Thread", thread_id, "starpu_STATE_40");
}
void handleEZTRACE_starpu_starpu_data_unregister_no_coherency_82() {
    FUNC_NAME;
    DECLARE_THREAD_ID_STR(thread_id, CUR_INDEX, CUR_THREAD_ID);
    CHANGE() popState (CURRENT, "ST_Thread", thread_id);
}
void handleEZTRACE_starpu_starpu_data_unregister_submit_83() {
    FUNC_NAME;
    DECLARE_THREAD_ID_STR(thread_id, CUR_INDEX, CUR_THREAD_ID);
    CHANGE() pushState (CURRENT, "ST_Thread", thread_id, "starpu_STATE_41");
}
void handleEZTRACE_starpu_starpu_data_unregister_submit_84() {
    FUNC_NAME;
    DECLARE_THREAD_ID_STR(thread_id, CUR_INDEX, CUR_THREAD_ID);
    CHANGE() popState (CURRENT, "ST_Thread", thread_id);
}
void handleEZTRACE_starpu_starpu_data_invalidate_85() {
    FUNC_NAME;
    DECLARE_THREAD_ID_STR(thread_id, CUR_INDEX, CUR_THREAD_ID);
    CHANGE() pushState (CURRENT, "ST_Thread", thread_id, "starpu_STATE_42");
}
void handleEZTRACE_starpu_starpu_data_invalidate_86() {
    FUNC_NAME;
    DECLARE_THREAD_ID_STR(thread_id, CUR_INDEX, CUR_THREAD_ID);
    CHANGE() popState (CURRENT, "ST_Thread", thread_id);
}
void handleEZTRACE_starpu_starpu_data_invalidate_submit_87() {
    FUNC_NAME;
    DECLARE_THREAD_ID_STR(thread_id, CUR_INDEX, CUR_THREAD_ID);
    CHANGE() pushState (CURRENT, "ST_Thread", thread_id, "starpu_STATE_43");
}
void handleEZTRACE_starpu_starpu_data_invalidate_submit_88() {
    FUNC_NAME;
    DECLARE_THREAD_ID_STR(thread_id, CUR_INDEX, CUR_THREAD_ID);
    CHANGE() popState (CURRENT, "ST_Thread", thread_id);
}
void handleEZTRACE_starpu_starpu_data_acquire_91() {
    FUNC_NAME;
    DECLARE_THREAD_ID_STR(thread_id, CUR_INDEX, CUR_THREAD_ID);
    CHANGE() pushState (CURRENT, "ST_Thread", thread_id, "starpu_STATE_45");
}
void handleEZTRACE_starpu_starpu_data_acquire_92() {
    FUNC_NAME;
    DECLARE_THREAD_ID_STR(thread_id, CUR_INDEX, CUR_THREAD_ID);
    CHANGE() popState (CURRENT, "ST_Thread", thread_id);
}
void handleEZTRACE_starpu_starpu_data_acquire_on_node_93() {
    FUNC_NAME;
    DECLARE_THREAD_ID_STR(thread_id, CUR_INDEX, CUR_THREAD_ID);
    CHANGE() pushState (CURRENT, "ST_Thread", thread_id, "starpu_STATE_46");
}
void handleEZTRACE_starpu_starpu_data_acquire_on_node_94() {
    FUNC_NAME;
    DECLARE_THREAD_ID_STR(thread_id, CUR_INDEX, CUR_THREAD_ID);
    CHANGE() popState (CURRENT, "ST_Thread", thread_id);
}
void handleEZTRACE_starpu_starpu_data_acquire_cb_95() {
    FUNC_NAME;
    DECLARE_THREAD_ID_STR(thread_id, CUR_INDEX, CUR_THREAD_ID);
    CHANGE() pushState (CURRENT, "ST_Thread", thread_id, "starpu_STATE_47");
}
void handleEZTRACE_starpu_starpu_data_acquire_cb_96() {
    FUNC_NAME;
    DECLARE_THREAD_ID_STR(thread_id, CUR_INDEX, CUR_THREAD_ID);
    CHANGE() popState (CURRENT, "ST_Thread", thread_id);
}
void handleEZTRACE_starpu_starpu_data_acquire_on_node_cb_97() {
    FUNC_NAME;
    DECLARE_THREAD_ID_STR(thread_id, CUR_INDEX, CUR_THREAD_ID);
    CHANGE() pushState (CURRENT, "ST_Thread", thread_id, "starpu_STATE_48");
}
void handleEZTRACE_starpu_starpu_data_acquire_on_node_cb_98() {
    FUNC_NAME;
    DECLARE_THREAD_ID_STR(thread_id, CUR_INDEX, CUR_THREAD_ID);
    CHANGE() popState (CURRENT, "ST_Thread", thread_id);
}
void handleEZTRACE_starpu_starpu_data_release_99() {
    FUNC_NAME;
    DECLARE_THREAD_ID_STR(thread_id, CUR_INDEX, CUR_THREAD_ID);
    CHANGE() pushState (CURRENT, "ST_Thread", thread_id, "starpu_STATE_49");
}
void handleEZTRACE_starpu_starpu_data_release_100() {
    FUNC_NAME;
    DECLARE_THREAD_ID_STR(thread_id, CUR_INDEX, CUR_THREAD_ID);
    CHANGE() popState (CURRENT, "ST_Thread", thread_id);
}
void handleEZTRACE_starpu_starpu_data_release_on_node_101() {
    FUNC_NAME;
    DECLARE_THREAD_ID_STR(thread_id, CUR_INDEX, CUR_THREAD_ID);
    CHANGE() pushState (CURRENT, "ST_Thread", thread_id, "starpu_STATE_50");
}
void handleEZTRACE_starpu_starpu_data_release_on_node_102() {
    FUNC_NAME;
    DECLARE_THREAD_ID_STR(thread_id, CUR_INDEX, CUR_THREAD_ID);
    CHANGE() popState (CURRENT, "ST_Thread", thread_id);
}
void handleEZTRACE_starpu_starpu_malloc_on_node_183() {
    FUNC_NAME;
    DECLARE_THREAD_ID_STR(thread_id, CUR_INDEX, CUR_THREAD_ID);
    CHANGE() pushState (CURRENT, "ST_Thread", thread_id, "starpu_STATE_91");
}
void handleEZTRACE_starpu_starpu_malloc_on_node_184() {
    FUNC_NAME;
    DECLARE_THREAD_ID_STR(thread_id, CUR_INDEX, CUR_THREAD_ID);
    CHANGE() popState (CURRENT, "ST_Thread", thread_id);
}
void handleEZTRACE_starpu_starpu_free_on_node_185() {
    FUNC_NAME;
    DECLARE_THREAD_ID_STR(thread_id, CUR_INDEX, CUR_THREAD_ID);
    CHANGE() pushState (CURRENT, "ST_Thread", thread_id, "starpu_STATE_92");
}
void handleEZTRACE_starpu_starpu_free_on_node_186() {
    FUNC_NAME;
    DECLARE_THREAD_ID_STR(thread_id, CUR_INDEX, CUR_THREAD_ID);
    CHANGE() popState (CURRENT, "ST_Thread", thread_id);
}
void handleEZTRACE_starpu_starpu_mpi_isend_311() {
    FUNC_NAME;
    DECLARE_THREAD_ID_STR(thread_id, CUR_INDEX, CUR_THREAD_ID);
    CHANGE() pushState (CURRENT, "ST_Thread", thread_id, "starpu_STATE_155");
}
void handleEZTRACE_starpu_starpu_mpi_isend_312() {
    FUNC_NAME;
    DECLARE_THREAD_ID_STR(thread_id, CUR_INDEX, CUR_THREAD_ID);
    CHANGE() popState (CURRENT, "ST_Thread", thread_id);
}
void handleEZTRACE_starpu_starpu_mpi_irecv_313() {
    FUNC_NAME;
    DECLARE_THREAD_ID_STR(thread_id, CUR_INDEX, CUR_THREAD_ID);
    CHANGE() pushState (CURRENT, "ST_Thread", thread_id, "starpu_STATE_156");
}
void handleEZTRACE_starpu_starpu_mpi_irecv_314() {
    FUNC_NAME;
    DECLARE_THREAD_ID_STR(thread_id, CUR_INDEX, CUR_THREAD_ID);
    CHANGE() popState (CURRENT, "ST_Thread", thread_id);
}
void handleEZTRACE_starpu_starpu_mpi_send_315() {
    FUNC_NAME;
    DECLARE_THREAD_ID_STR(thread_id, CUR_INDEX, CUR_THREAD_ID);
    CHANGE() pushState (CURRENT, "ST_Thread", thread_id, "starpu_STATE_157");
}
void handleEZTRACE_starpu_starpu_mpi_send_316() {
    FUNC_NAME;
    DECLARE_THREAD_ID_STR(thread_id, CUR_INDEX, CUR_THREAD_ID);
    CHANGE() popState (CURRENT, "ST_Thread", thread_id);
}
void handleEZTRACE_starpu_starpu_mpi_recv_317() {
    FUNC_NAME;
    DECLARE_THREAD_ID_STR(thread_id, CUR_INDEX, CUR_THREAD_ID);
    CHANGE() pushState (CURRENT, "ST_Thread", thread_id, "starpu_STATE_158");
}
void handleEZTRACE_starpu_starpu_mpi_recv_318() {
    FUNC_NAME;
    DECLARE_THREAD_ID_STR(thread_id, CUR_INDEX, CUR_THREAD_ID);
    CHANGE() popState (CURRENT, "ST_Thread", thread_id);
}
void handleEZTRACE_starpu_starpu_mpi_isend_detached_319() {
    FUNC_NAME;
    DECLARE_THREAD_ID_STR(thread_id, CUR_INDEX, CUR_THREAD_ID);
    CHANGE() pushState (CURRENT, "ST_Thread", thread_id, "starpu_STATE_159");
}
void handleEZTRACE_starpu_starpu_mpi_isend_detached_320() {
    FUNC_NAME;
    DECLARE_THREAD_ID_STR(thread_id, CUR_INDEX, CUR_THREAD_ID);
    CHANGE() popState (CURRENT, "ST_Thread", thread_id);
}
void handleEZTRACE_starpu_starpu_mpi_irecv_detached_321() {
    FUNC_NAME;
    DECLARE_THREAD_ID_STR(thread_id, CUR_INDEX, CUR_THREAD_ID);
    CHANGE() pushState (CURRENT, "ST_Thread", thread_id, "starpu_STATE_160");
}
void handleEZTRACE_starpu_starpu_mpi_irecv_detached_322() {
    FUNC_NAME;
    DECLARE_THREAD_ID_STR(thread_id, CUR_INDEX, CUR_THREAD_ID);
    CHANGE() popState (CURRENT, "ST_Thread", thread_id);
}
void handleEZTRACE_starpu_starpu_mpi_wait_323() {
    FUNC_NAME;
    DECLARE_THREAD_ID_STR(thread_id, CUR_INDEX, CUR_THREAD_ID);
    CHANGE() pushState (CURRENT, "ST_Thread", thread_id, "starpu_STATE_161");
}
void handleEZTRACE_starpu_starpu_mpi_wait_324() {
    FUNC_NAME;
    DECLARE_THREAD_ID_STR(thread_id, CUR_INDEX, CUR_THREAD_ID);
    CHANGE() popState (CURRENT, "ST_Thread", thread_id);
}
void handleEZTRACE_starpu_starpu_mpi_test_325() {
    FUNC_NAME;
    DECLARE_THREAD_ID_STR(thread_id, CUR_INDEX, CUR_THREAD_ID);
    CHANGE() pushState (CURRENT, "ST_Thread", thread_id, "starpu_STATE_162");
}
void handleEZTRACE_starpu_starpu_mpi_test_326() {
    FUNC_NAME;
    DECLARE_THREAD_ID_STR(thread_id, CUR_INDEX, CUR_THREAD_ID);
    CHANGE() popState (CURRENT, "ST_Thread", thread_id);
}
void handleEZTRACE_starpu_starpu_mpi_barrier_327() {
    FUNC_NAME;
    DECLARE_THREAD_ID_STR(thread_id, CUR_INDEX, CUR_THREAD_ID);
    CHANGE() pushState (CURRENT, "ST_Thread", thread_id, "starpu_STATE_163");
}
void handleEZTRACE_starpu_starpu_mpi_barrier_328() {
    FUNC_NAME;
    DECLARE_THREAD_ID_STR(thread_id, CUR_INDEX, CUR_THREAD_ID);
    CHANGE() popState (CURRENT, "ST_Thread", thread_id);
}
void handleEZTRACE_starpu_starpu_mpi_init_329() {
    FUNC_NAME;
    DECLARE_THREAD_ID_STR(thread_id, CUR_INDEX, CUR_THREAD_ID);
    CHANGE() pushState (CURRENT, "ST_Thread", thread_id, "starpu_STATE_164");
}
void handleEZTRACE_starpu_starpu_mpi_init_330() {
    FUNC_NAME;
    DECLARE_THREAD_ID_STR(thread_id, CUR_INDEX, CUR_THREAD_ID);
    CHANGE() popState (CURRENT, "ST_Thread", thread_id);
}
void handleEZTRACE_starpu_starpu_mpi_initialize_331() {
    FUNC_NAME;
    DECLARE_THREAD_ID_STR(thread_id, CUR_INDEX, CUR_THREAD_ID);
    CHANGE() pushState (CURRENT, "ST_Thread", thread_id, "starpu_STATE_165");
}
void handleEZTRACE_starpu_starpu_mpi_initialize_332() {
    FUNC_NAME;
    DECLARE_THREAD_ID_STR(thread_id, CUR_INDEX, CUR_THREAD_ID);
    CHANGE() popState (CURRENT, "ST_Thread", thread_id);
}
void handleEZTRACE_starpu_starpu_mpi_initialize_extended_333() {
    FUNC_NAME;
    DECLARE_THREAD_ID_STR(thread_id, CUR_INDEX, CUR_THREAD_ID);
    CHANGE() pushState (CURRENT, "ST_Thread", thread_id, "starpu_STATE_166");
}
void handleEZTRACE_starpu_starpu_mpi_initialize_extended_334() {
    FUNC_NAME;
    DECLARE_THREAD_ID_STR(thread_id, CUR_INDEX, CUR_THREAD_ID);
    CHANGE() popState (CURRENT, "ST_Thread", thread_id);
}
void handleEZTRACE_starpu_starpu_mpi_shutdown_335() {
    FUNC_NAME;
    DECLARE_THREAD_ID_STR(thread_id, CUR_INDEX, CUR_THREAD_ID);
    CHANGE() pushState (CURRENT, "ST_Thread", thread_id, "starpu_STATE_167");
}
void handleEZTRACE_starpu_starpu_mpi_shutdown_336() {
    FUNC_NAME;
    DECLARE_THREAD_ID_STR(thread_id, CUR_INDEX, CUR_THREAD_ID);
    CHANGE() popState (CURRENT, "ST_Thread", thread_id);
}
void handleEZTRACE_starpu_starpu_mpi_get_data_on_node_337() {
    FUNC_NAME;
    DECLARE_THREAD_ID_STR(thread_id, CUR_INDEX, CUR_THREAD_ID);
    CHANGE() pushState (CURRENT, "ST_Thread", thread_id, "starpu_STATE_168");
}
void handleEZTRACE_starpu_starpu_mpi_get_data_on_node_338() {
    FUNC_NAME;
    DECLARE_THREAD_ID_STR(thread_id, CUR_INDEX, CUR_THREAD_ID);
    CHANGE() popState (CURRENT, "ST_Thread", thread_id);
}
void handleEZTRACE_starpu_starpu_mpi_get_data_on_node_detached_339() {
    FUNC_NAME;
    DECLARE_THREAD_ID_STR(thread_id, CUR_INDEX, CUR_THREAD_ID);
    CHANGE() pushState (CURRENT, "ST_Thread", thread_id, "starpu_STATE_169");
}
void handleEZTRACE_starpu_starpu_mpi_get_data_on_node_detached_340() {
    FUNC_NAME;
    DECLARE_THREAD_ID_STR(thread_id, CUR_INDEX, CUR_THREAD_ID);
    CHANGE() popState (CURRENT, "ST_Thread", thread_id);
}
void handleEZTRACE_starpu_starpu_mpi_redux_data_341() {
    FUNC_NAME;
    DECLARE_THREAD_ID_STR(thread_id, CUR_INDEX, CUR_THREAD_ID);
    CHANGE() pushState (CURRENT, "ST_Thread", thread_id, "starpu_STATE_170");
}
void handleEZTRACE_starpu_starpu_mpi_redux_data_342() {
    FUNC_NAME;
    DECLARE_THREAD_ID_STR(thread_id, CUR_INDEX, CUR_THREAD_ID);
    CHANGE() popState (CURRENT, "ST_Thread", thread_id);
}
void handleEZTRACE_starpu_starpu_mpi_scatter_detached_343() {
    FUNC_NAME;
    DECLARE_THREAD_ID_STR(thread_id, CUR_INDEX, CUR_THREAD_ID);
    CHANGE() pushState (CURRENT, "ST_Thread", thread_id, "starpu_STATE_171");
}
void handleEZTRACE_starpu_starpu_mpi_scatter_detached_344() {
    FUNC_NAME;
    DECLARE_THREAD_ID_STR(thread_id, CUR_INDEX, CUR_THREAD_ID);
    CHANGE() popState (CURRENT, "ST_Thread", thread_id);
}
void handleEZTRACE_starpu_starpu_mpi_gather_detached_345() {
    FUNC_NAME;
    DECLARE_THREAD_ID_STR(thread_id, CUR_INDEX, CUR_THREAD_ID);
    CHANGE() pushState (CURRENT, "ST_Thread", thread_id, "starpu_STATE_172");
}
void handleEZTRACE_starpu_starpu_mpi_gather_detached_346() {
    FUNC_NAME;
    DECLARE_THREAD_ID_STR(thread_id, CUR_INDEX, CUR_THREAD_ID);
    CHANGE() popState (CURRENT, "ST_Thread", thread_id);
}
void handleEZTRACE_starpu_starpu_mpi_isend_detached_unlock_tag_347() {
    FUNC_NAME;
    DECLARE_THREAD_ID_STR(thread_id, CUR_INDEX, CUR_THREAD_ID);
    CHANGE() pushState (CURRENT, "ST_Thread", thread_id, "starpu_STATE_173");
}
void handleEZTRACE_starpu_starpu_mpi_isend_detached_unlock_tag_348() {
    FUNC_NAME;
    DECLARE_THREAD_ID_STR(thread_id, CUR_INDEX, CUR_THREAD_ID);
    CHANGE() popState (CURRENT, "ST_Thread", thread_id);
}
void handleEZTRACE_starpu_starpu_mpi_irecv_detached_unlock_tag_349() {
    FUNC_NAME;
    DECLARE_THREAD_ID_STR(thread_id, CUR_INDEX, CUR_THREAD_ID);
    CHANGE() pushState (CURRENT, "ST_Thread", thread_id, "starpu_STATE_174");
}
void handleEZTRACE_starpu_starpu_mpi_irecv_detached_unlock_tag_350() {
    FUNC_NAME;
    DECLARE_THREAD_ID_STR(thread_id, CUR_INDEX, CUR_THREAD_ID);
    CHANGE() popState (CURRENT, "ST_Thread", thread_id);
}
void handleEZTRACE_starpu_starpu_mpi_isend_array_detached_unlock_tag_351() {
    FUNC_NAME;
    DECLARE_THREAD_ID_STR(thread_id, CUR_INDEX, CUR_THREAD_ID);
    CHANGE() pushState (CURRENT, "ST_Thread", thread_id, "starpu_STATE_175");
}
void handleEZTRACE_starpu_starpu_mpi_isend_array_detached_unlock_tag_352() {
    FUNC_NAME;
    DECLARE_THREAD_ID_STR(thread_id, CUR_INDEX, CUR_THREAD_ID);
    CHANGE() popState (CURRENT, "ST_Thread", thread_id);
}
void handleEZTRACE_starpu_starpu_mpi_irecv_array_detached_unlock_tag_353() {
    FUNC_NAME;
    DECLARE_THREAD_ID_STR(thread_id, CUR_INDEX, CUR_THREAD_ID);
    CHANGE() pushState (CURRENT, "ST_Thread", thread_id, "starpu_STATE_176");
}
void handleEZTRACE_starpu_starpu_mpi_irecv_array_detached_unlock_tag_354() {
    FUNC_NAME;
    DECLARE_THREAD_ID_STR(thread_id, CUR_INDEX, CUR_THREAD_ID);
    CHANGE() popState (CURRENT, "ST_Thread", thread_id);
}
void handleEZTRACE_starpu_starpu_mpi_comm_amounts_retrieve_355() {
    FUNC_NAME;
    DECLARE_THREAD_ID_STR(thread_id, CUR_INDEX, CUR_THREAD_ID);
    CHANGE() pushState (CURRENT, "ST_Thread", thread_id, "starpu_STATE_177");
}
void handleEZTRACE_starpu_starpu_mpi_comm_amounts_retrieve_356() {
    FUNC_NAME;
    DECLARE_THREAD_ID_STR(thread_id, CUR_INDEX, CUR_THREAD_ID);
    CHANGE() popState (CURRENT, "ST_Thread", thread_id);
}
void handleEZTRACE_starpu_starpu_mpi_cache_flush_357() {
    FUNC_NAME;
    DECLARE_THREAD_ID_STR(thread_id, CUR_INDEX, CUR_THREAD_ID);
    CHANGE() pushState (CURRENT, "ST_Thread", thread_id, "starpu_STATE_178");
}
void handleEZTRACE_starpu_starpu_mpi_cache_flush_358() {
    FUNC_NAME;
    DECLARE_THREAD_ID_STR(thread_id, CUR_INDEX, CUR_THREAD_ID);
    CHANGE() popState (CURRENT, "ST_Thread", thread_id);
}
void handleEZTRACE_starpu_starpu_mpi_cache_flush_all_data_359() {
    FUNC_NAME;
    DECLARE_THREAD_ID_STR(thread_id, CUR_INDEX, CUR_THREAD_ID);
    CHANGE() pushState (CURRENT, "ST_Thread", thread_id, "starpu_STATE_179");
}
void handleEZTRACE_starpu_starpu_mpi_cache_flush_all_data_360() {
    FUNC_NAME;
    DECLARE_THREAD_ID_STR(thread_id, CUR_INDEX, CUR_THREAD_ID);
    CHANGE() popState (CURRENT, "ST_Thread", thread_id);
}
void handleEZTRACE_starpu_starpu_mpi_data_register_361() {
    FUNC_NAME;
    DECLARE_THREAD_ID_STR(thread_id, CUR_INDEX, CUR_THREAD_ID);
    CHANGE() pushState (CURRENT, "ST_Thread", thread_id, "starpu_STATE_180");
}
void handleEZTRACE_starpu_starpu_mpi_data_register_362() {
    FUNC_NAME;
    DECLARE_THREAD_ID_STR(thread_id, CUR_INDEX, CUR_THREAD_ID);
    CHANGE() popState (CURRENT, "ST_Thread", thread_id);
}
void handleEZTRACE_starpu_starpu_sched_ctx_add_workers_411() {
    FUNC_NAME;
    DECLARE_THREAD_ID_STR(thread_id, CUR_INDEX, CUR_THREAD_ID);
    CHANGE() pushState (CURRENT, "ST_Thread", thread_id, "starpu_STATE_205");
}
void handleEZTRACE_starpu_starpu_sched_ctx_add_workers_412() {
    FUNC_NAME;
    DECLARE_THREAD_ID_STR(thread_id, CUR_INDEX, CUR_THREAD_ID);
    CHANGE() popState (CURRENT, "ST_Thread", thread_id);
}
void handleEZTRACE_starpu_starpu_sched_ctx_remove_workers_413() {
    FUNC_NAME;
    DECLARE_THREAD_ID_STR(thread_id, CUR_INDEX, CUR_THREAD_ID);
    CHANGE() pushState (CURRENT, "ST_Thread", thread_id, "starpu_STATE_206");
}
void handleEZTRACE_starpu_starpu_sched_ctx_remove_workers_414() {
    FUNC_NAME;
    DECLARE_THREAD_ID_STR(thread_id, CUR_INDEX, CUR_THREAD_ID);
    CHANGE() popState (CURRENT, "ST_Thread", thread_id);
}
void handleEZTRACE_starpu_starpu_sched_ctx_delete_415() {
    FUNC_NAME;
    DECLARE_THREAD_ID_STR(thread_id, CUR_INDEX, CUR_THREAD_ID);
    CHANGE() pushState (CURRENT, "ST_Thread", thread_id, "starpu_STATE_207");
}
void handleEZTRACE_starpu_starpu_sched_ctx_delete_416() {
    FUNC_NAME;
    DECLARE_THREAD_ID_STR(thread_id, CUR_INDEX, CUR_THREAD_ID);
    CHANGE() popState (CURRENT, "ST_Thread", thread_id);
}
void handleEZTRACE_starpu_starpu_sched_ctx_set_inheritor_417() {
    FUNC_NAME;
    DECLARE_THREAD_ID_STR(thread_id, CUR_INDEX, CUR_THREAD_ID);
    CHANGE() pushState (CURRENT, "ST_Thread", thread_id, "starpu_STATE_208");
}
void handleEZTRACE_starpu_starpu_sched_ctx_set_inheritor_418() {
    FUNC_NAME;
    DECLARE_THREAD_ID_STR(thread_id, CUR_INDEX, CUR_THREAD_ID);
    CHANGE() popState (CURRENT, "ST_Thread", thread_id);
}
void handleEZTRACE_starpu_starpu_sched_ctx_set_context_419() {
    FUNC_NAME;
    DECLARE_THREAD_ID_STR(thread_id, CUR_INDEX, CUR_THREAD_ID);
    CHANGE() pushState (CURRENT, "ST_Thread", thread_id, "starpu_STATE_209");
}
void handleEZTRACE_starpu_starpu_sched_ctx_set_context_420() {
    FUNC_NAME;
    DECLARE_THREAD_ID_STR(thread_id, CUR_INDEX, CUR_THREAD_ID);
    CHANGE() popState (CURRENT, "ST_Thread", thread_id);
}
void handleEZTRACE_starpu_starpu_sched_ctx_get_context_421() {
    FUNC_NAME;
    DECLARE_THREAD_ID_STR(thread_id, CUR_INDEX, CUR_THREAD_ID);
    CHANGE() pushState (CURRENT, "ST_Thread", thread_id, "starpu_STATE_210");
}
void handleEZTRACE_starpu_starpu_sched_ctx_get_context_422() {
    FUNC_NAME;
    DECLARE_THREAD_ID_STR(thread_id, CUR_INDEX, CUR_THREAD_ID);
    CHANGE() popState (CURRENT, "ST_Thread", thread_id);
}
void handleEZTRACE_starpu_starpu_sched_ctx_stop_task_submission_423() {
    FUNC_NAME;
    DECLARE_THREAD_ID_STR(thread_id, CUR_INDEX, CUR_THREAD_ID);
    CHANGE() pushState (CURRENT, "ST_Thread", thread_id, "starpu_STATE_211");
}
void handleEZTRACE_starpu_starpu_sched_ctx_stop_task_submission_424() {
    FUNC_NAME;
    DECLARE_THREAD_ID_STR(thread_id, CUR_INDEX, CUR_THREAD_ID);
    CHANGE() popState (CURRENT, "ST_Thread", thread_id);
}
void handleEZTRACE_starpu_starpu_sched_ctx_finished_submit_425() {
    FUNC_NAME;
    DECLARE_THREAD_ID_STR(thread_id, CUR_INDEX, CUR_THREAD_ID);
    CHANGE() pushState (CURRENT, "ST_Thread", thread_id, "starpu_STATE_212");
}
void handleEZTRACE_starpu_starpu_sched_ctx_finished_submit_426() {
    FUNC_NAME;
    DECLARE_THREAD_ID_STR(thread_id, CUR_INDEX, CUR_THREAD_ID);
    CHANGE() popState (CURRENT, "ST_Thread", thread_id);
}
void handleEZTRACE_starpu_starpu_sched_ctx_create_worker_collection_461() {
    FUNC_NAME;
    DECLARE_THREAD_ID_STR(thread_id, CUR_INDEX, CUR_THREAD_ID);
    CHANGE() pushState (CURRENT, "ST_Thread", thread_id, "starpu_STATE_230");
}
void handleEZTRACE_starpu_starpu_sched_ctx_create_worker_collection_462() {
    FUNC_NAME;
    DECLARE_THREAD_ID_STR(thread_id, CUR_INDEX, CUR_THREAD_ID);
    CHANGE() popState (CURRENT, "ST_Thread", thread_id);
}
void handleEZTRACE_starpu_starpu_sched_ctx_delete_worker_collection_463() {
    FUNC_NAME;
    DECLARE_THREAD_ID_STR(thread_id, CUR_INDEX, CUR_THREAD_ID);
    CHANGE() pushState (CURRENT, "ST_Thread", thread_id, "starpu_STATE_231");
}
void handleEZTRACE_starpu_starpu_sched_ctx_delete_worker_collection_464() {
    FUNC_NAME;
    DECLARE_THREAD_ID_STR(thread_id, CUR_INDEX, CUR_THREAD_ID);
    CHANGE() popState (CURRENT, "ST_Thread", thread_id);
}
void handleEZTRACE_starpu_starpu_sched_ctx_get_worker_collection_465() {
    FUNC_NAME;
    DECLARE_THREAD_ID_STR(thread_id, CUR_INDEX, CUR_THREAD_ID);
    CHANGE() pushState (CURRENT, "ST_Thread", thread_id, "starpu_STATE_232");
}
void handleEZTRACE_starpu_starpu_sched_ctx_get_worker_collection_466() {
    FUNC_NAME;
    DECLARE_THREAD_ID_STR(thread_id, CUR_INDEX, CUR_THREAD_ID);
    CHANGE() popState (CURRENT, "ST_Thread", thread_id);
}
void handleEZTRACE_starpu_starpu_prefetch_task_input_on_node_495() {
    FUNC_NAME;
    DECLARE_THREAD_ID_STR(thread_id, CUR_INDEX, CUR_THREAD_ID);
    CHANGE() pushState (CURRENT, "ST_Thread", thread_id, "starpu_STATE_247");
}
void handleEZTRACE_starpu_starpu_prefetch_task_input_on_node_496() {
    FUNC_NAME;
    DECLARE_THREAD_ID_STR(thread_id, CUR_INDEX, CUR_THREAD_ID);
    CHANGE() popState (CURRENT, "ST_Thread", thread_id);
}
void handleEZTRACE_starpu_starpu_malloc_521() {
    FUNC_NAME;
    DECLARE_THREAD_ID_STR(thread_id, CUR_INDEX, CUR_THREAD_ID);
    CHANGE() pushState (CURRENT, "ST_Thread", thread_id, "starpu_STATE_260");
}
void handleEZTRACE_starpu_starpu_malloc_522() {
    FUNC_NAME;
    DECLARE_THREAD_ID_STR(thread_id, CUR_INDEX, CUR_THREAD_ID);
    CHANGE() popState (CURRENT, "ST_Thread", thread_id);
}
void handleEZTRACE_starpu_starpu_free_523() {
    FUNC_NAME;
    DECLARE_THREAD_ID_STR(thread_id, CUR_INDEX, CUR_THREAD_ID);
    CHANGE() pushState (CURRENT, "ST_Thread", thread_id, "starpu_STATE_261");
}
void handleEZTRACE_starpu_starpu_free_524() {
    FUNC_NAME;
    DECLARE_THREAD_ID_STR(thread_id, CUR_INDEX, CUR_THREAD_ID);
    CHANGE() popState (CURRENT, "ST_Thread", thread_id);
}
void handleEZTRACE_starpu_starpu_malloc_flags_525() {
    FUNC_NAME;
    DECLARE_THREAD_ID_STR(thread_id, CUR_INDEX, CUR_THREAD_ID);
    CHANGE() pushState (CURRENT, "ST_Thread", thread_id, "starpu_STATE_262");
}
void handleEZTRACE_starpu_starpu_malloc_flags_526() {
    FUNC_NAME;
    DECLARE_THREAD_ID_STR(thread_id, CUR_INDEX, CUR_THREAD_ID);
    CHANGE() popState (CURRENT, "ST_Thread", thread_id);
}
void handleEZTRACE_starpu_starpu_free_flags_527() {
    FUNC_NAME;
    DECLARE_THREAD_ID_STR(thread_id, CUR_INDEX, CUR_THREAD_ID);
    CHANGE() pushState (CURRENT, "ST_Thread", thread_id, "starpu_STATE_263");
}
void handleEZTRACE_starpu_starpu_free_flags_528() {
    FUNC_NAME;
    DECLARE_THREAD_ID_STR(thread_id, CUR_INDEX, CUR_THREAD_ID);
    CHANGE() popState (CURRENT, "ST_Thread", thread_id);
}
void handleEZTRACE_starpu_starpu_tag_wait_537() {
    FUNC_NAME;
    DECLARE_THREAD_ID_STR(thread_id, CUR_INDEX, CUR_THREAD_ID);
    CHANGE() pushState (CURRENT, "ST_Thread", thread_id, "starpu_STATE_268");
}
void handleEZTRACE_starpu_starpu_tag_wait_538() {
    FUNC_NAME;
    DECLARE_THREAD_ID_STR(thread_id, CUR_INDEX, CUR_THREAD_ID);
    CHANGE() popState (CURRENT, "ST_Thread", thread_id);
}
void handleEZTRACE_starpu_starpu_tag_wait_array_539() {
    FUNC_NAME;
    DECLARE_THREAD_ID_STR(thread_id, CUR_INDEX, CUR_THREAD_ID);
    CHANGE() pushState (CURRENT, "ST_Thread", thread_id, "starpu_STATE_269");
}
void handleEZTRACE_starpu_starpu_tag_wait_array_540() {
    FUNC_NAME;
    DECLARE_THREAD_ID_STR(thread_id, CUR_INDEX, CUR_THREAD_ID);
    CHANGE() popState (CURRENT, "ST_Thread", thread_id);
}
void handleEZTRACE_starpu_starpu_tag_notify_from_apps_541() {
    FUNC_NAME;
    DECLARE_THREAD_ID_STR(thread_id, CUR_INDEX, CUR_THREAD_ID);
    CHANGE() pushState (CURRENT, "ST_Thread", thread_id, "starpu_STATE_270");
}
void handleEZTRACE_starpu_starpu_tag_notify_from_apps_542() {
    FUNC_NAME;
    DECLARE_THREAD_ID_STR(thread_id, CUR_INDEX, CUR_THREAD_ID);
    CHANGE() popState (CURRENT, "ST_Thread", thread_id);
}
void handleEZTRACE_starpu_starpu_task_init_547() {
    FUNC_NAME;
    DECLARE_THREAD_ID_STR(thread_id, CUR_INDEX, CUR_THREAD_ID);
    CHANGE() pushState (CURRENT, "ST_Thread", thread_id, "starpu_STATE_273");
}
void handleEZTRACE_starpu_starpu_task_init_548() {
    FUNC_NAME;
    DECLARE_THREAD_ID_STR(thread_id, CUR_INDEX, CUR_THREAD_ID);
    CHANGE() popState (CURRENT, "ST_Thread", thread_id);
}
void handleEZTRACE_starpu_starpu_task_clean_549() {
    FUNC_NAME;
    DECLARE_THREAD_ID_STR(thread_id, CUR_INDEX, CUR_THREAD_ID);
    CHANGE() pushState (CURRENT, "ST_Thread", thread_id, "starpu_STATE_274");
}
void handleEZTRACE_starpu_starpu_task_clean_550() {
    FUNC_NAME;
    DECLARE_THREAD_ID_STR(thread_id, CUR_INDEX, CUR_THREAD_ID);
    CHANGE() popState (CURRENT, "ST_Thread", thread_id);
}
void handleEZTRACE_starpu_starpu_task_create_551() {
    FUNC_NAME;
    DECLARE_THREAD_ID_STR(thread_id, CUR_INDEX, CUR_THREAD_ID);
    CHANGE() pushState (CURRENT, "ST_Thread", thread_id, "starpu_STATE_275");
}
void handleEZTRACE_starpu_starpu_task_create_552() {
    FUNC_NAME;
    DECLARE_THREAD_ID_STR(thread_id, CUR_INDEX, CUR_THREAD_ID);
    CHANGE() popState (CURRENT, "ST_Thread", thread_id);
}
void handleEZTRACE_starpu_starpu_task_destroy_553() {
    FUNC_NAME;
    DECLARE_THREAD_ID_STR(thread_id, CUR_INDEX, CUR_THREAD_ID);
    CHANGE() pushState (CURRENT, "ST_Thread", thread_id, "starpu_STATE_276");
}
void handleEZTRACE_starpu_starpu_task_destroy_554() {
    FUNC_NAME;
    DECLARE_THREAD_ID_STR(thread_id, CUR_INDEX, CUR_THREAD_ID);
    CHANGE() popState (CURRENT, "ST_Thread", thread_id);
}

void handleEZTRACE_starpu_starpu_task_submit_555() {
    FUNC_NAME;
    DECLARE_THREAD_ID_STR(thread_id, CUR_INDEX, CUR_THREAD_ID);
    CHANGE() pushState (CURRENT, "ST_Thread", thread_id, "starpu_STATE_277");
}

void handleEZTRACE_starpu_starpu_task_submit_556() {
    FUNC_NAME;
    DECLARE_THREAD_ID_STR(thread_id, CUR_INDEX, CUR_THREAD_ID);
    char *proc = calloc(1, sizeof(char) * 9);
    sprintf(proc, "CT_SP#%i", CUR_RANK);
    CHANGE() addVar (CURRENT, "starpu_VAR__0", proc, (varPrec) 1);
    CHANGE() popState (CURRENT, "ST_Thread", thread_id);
    //   free(proc);
}

void handleEZTRACE_starpu_starpu_task_submit_to_ctx_557() {
    FUNC_NAME;
    DECLARE_THREAD_ID_STR(thread_id, CUR_INDEX, CUR_THREAD_ID);
    CHANGE() pushState (CURRENT, "ST_Thread", thread_id, "starpu_STATE_278");
}
void handleEZTRACE_starpu_starpu_task_submit_to_ctx_558() {
    FUNC_NAME;
    DECLARE_THREAD_ID_STR(thread_id, CUR_INDEX, CUR_THREAD_ID);
    CHANGE() popState (CURRENT, "ST_Thread", thread_id);
}
void handleEZTRACE_starpu_starpu_task_wait_559() {
    FUNC_NAME;
    DECLARE_THREAD_ID_STR(thread_id, CUR_INDEX, CUR_THREAD_ID);
    CHANGE() pushState (CURRENT, "ST_Thread", thread_id, "starpu_STATE_279");
}
void handleEZTRACE_starpu_starpu_task_wait_560() {
    FUNC_NAME;
    DECLARE_THREAD_ID_STR(thread_id, CUR_INDEX, CUR_THREAD_ID);
    CHANGE() popState (CURRENT, "ST_Thread", thread_id);
}
void handleEZTRACE_starpu_starpu_task_wait_for_all_561() {
    FUNC_NAME;
    DECLARE_THREAD_ID_STR(thread_id, CUR_INDEX, CUR_THREAD_ID);
    CHANGE() pushState (CURRENT, "ST_Thread", thread_id, "starpu_STATE_280");
}
void handleEZTRACE_starpu_starpu_task_wait_for_all_562() {
    FUNC_NAME;
    DECLARE_THREAD_ID_STR(thread_id, CUR_INDEX, CUR_THREAD_ID);
    CHANGE() popState (CURRENT, "ST_Thread", thread_id);
}
void handleEZTRACE_starpu_starpu_task_wait_for_all_in_ctx_563() {
    FUNC_NAME;
    DECLARE_THREAD_ID_STR(thread_id, CUR_INDEX, CUR_THREAD_ID);
    CHANGE() pushState (CURRENT, "ST_Thread", thread_id, "starpu_STATE_281");
}
void handleEZTRACE_starpu_starpu_task_wait_for_all_in_ctx_564() {
    FUNC_NAME;
    DECLARE_THREAD_ID_STR(thread_id, CUR_INDEX, CUR_THREAD_ID);
    CHANGE() popState (CURRENT, "ST_Thread", thread_id);
}
void handleEZTRACE_starpu_starpu_task_wait_for_no_ready_565() {
    FUNC_NAME;
    DECLARE_THREAD_ID_STR(thread_id, CUR_INDEX, CUR_THREAD_ID);
    CHANGE() pushState (CURRENT, "ST_Thread", thread_id, "starpu_STATE_282");
}
void handleEZTRACE_starpu_starpu_task_wait_for_no_ready_566() {
    FUNC_NAME;
    DECLARE_THREAD_ID_STR(thread_id, CUR_INDEX, CUR_THREAD_ID);
    CHANGE() popState (CURRENT, "ST_Thread", thread_id);
}
void handleEZTRACE_starpu_starpu_task_nready_567() {
    FUNC_NAME;
    DECLARE_THREAD_ID_STR(thread_id, CUR_INDEX, CUR_THREAD_ID);
    CHANGE() pushState (CURRENT, "ST_Thread", thread_id, "starpu_STATE_283");
}
void handleEZTRACE_starpu_starpu_task_nready_568() {
    FUNC_NAME;
    DECLARE_THREAD_ID_STR(thread_id, CUR_INDEX, CUR_THREAD_ID);
    CHANGE() popState (CURRENT, "ST_Thread", thread_id);
}
void handleEZTRACE_starpu_starpu_task_nsubmitted_569() {
    FUNC_NAME;
    DECLARE_THREAD_ID_STR(thread_id, CUR_INDEX, CUR_THREAD_ID);
    CHANGE() pushState (CURRENT, "ST_Thread", thread_id, "starpu_STATE_284");
}
void handleEZTRACE_starpu_starpu_task_nsubmitted_570() {
    FUNC_NAME;
    DECLARE_THREAD_ID_STR(thread_id, CUR_INDEX, CUR_THREAD_ID);
    CHANGE() popState (CURRENT, "ST_Thread", thread_id);
}
void handleEZTRACE_starpu_starpu_task_dup_581() {
    FUNC_NAME;
    DECLARE_THREAD_ID_STR(thread_id, CUR_INDEX, CUR_THREAD_ID);
    CHANGE() pushState (CURRENT, "ST_Thread", thread_id, "starpu_STATE_290");
}
void handleEZTRACE_starpu_starpu_task_dup_582() {
    FUNC_NAME;
    DECLARE_THREAD_ID_STR(thread_id, CUR_INDEX, CUR_THREAD_ID);
    CHANGE() popState (CURRENT, "ST_Thread", thread_id);
}
void handleEZTRACE_starpu_starpu_task_bundle_create_587() {
    FUNC_NAME;
    DECLARE_THREAD_ID_STR(thread_id, CUR_INDEX, CUR_THREAD_ID);
    CHANGE() pushState (CURRENT, "ST_Thread", thread_id, "starpu_STATE_293");
}
void handleEZTRACE_starpu_starpu_task_bundle_create_588() {
    FUNC_NAME;
    DECLARE_THREAD_ID_STR(thread_id, CUR_INDEX, CUR_THREAD_ID);
    CHANGE() popState (CURRENT, "ST_Thread", thread_id);
}
void handleEZTRACE_starpu_starpu_task_bundle_insert_589() {
    FUNC_NAME;
    DECLARE_THREAD_ID_STR(thread_id, CUR_INDEX, CUR_THREAD_ID);
    CHANGE() pushState (CURRENT, "ST_Thread", thread_id, "starpu_STATE_294");
}
void handleEZTRACE_starpu_starpu_task_bundle_insert_590() {
    FUNC_NAME;
    DECLARE_THREAD_ID_STR(thread_id, CUR_INDEX, CUR_THREAD_ID);
    CHANGE() popState (CURRENT, "ST_Thread", thread_id);
}
void handleEZTRACE_starpu_starpu_task_bundle_remove_591() {
    FUNC_NAME;
    DECLARE_THREAD_ID_STR(thread_id, CUR_INDEX, CUR_THREAD_ID);
    CHANGE() pushState (CURRENT, "ST_Thread", thread_id, "starpu_STATE_295");
}
void handleEZTRACE_starpu_starpu_task_bundle_remove_592() {
    FUNC_NAME;
    DECLARE_THREAD_ID_STR(thread_id, CUR_INDEX, CUR_THREAD_ID);
    CHANGE() popState (CURRENT, "ST_Thread", thread_id);
}
void handleEZTRACE_starpu_starpu_task_bundle_close_593() {
    FUNC_NAME;
    DECLARE_THREAD_ID_STR(thread_id, CUR_INDEX, CUR_THREAD_ID);
    CHANGE() pushState (CURRENT, "ST_Thread", thread_id, "starpu_STATE_296");
}
void handleEZTRACE_starpu_starpu_task_bundle_close_594() {
    FUNC_NAME;
    DECLARE_THREAD_ID_STR(thread_id, CUR_INDEX, CUR_THREAD_ID);
    CHANGE() popState (CURRENT, "ST_Thread", thread_id);
}
void handleEZTRACE_starpu_starpu_create_sync_task_595() {
    FUNC_NAME;
    DECLARE_THREAD_ID_STR(thread_id, CUR_INDEX, CUR_THREAD_ID);
    CHANGE() pushState (CURRENT, "ST_Thread", thread_id, "starpu_STATE_297");
}
void handleEZTRACE_starpu_starpu_create_sync_task_596() {
    FUNC_NAME;
    DECLARE_THREAD_ID_STR(thread_id, CUR_INDEX, CUR_THREAD_ID);
    CHANGE() popState (CURRENT, "ST_Thread", thread_id);
}
void handleEZTRACE_starpu_starpu_execute_on_each_worker_691() {
    FUNC_NAME;
    DECLARE_THREAD_ID_STR(thread_id, CUR_INDEX, CUR_THREAD_ID);
    CHANGE() pushState (CURRENT, "ST_Thread", thread_id, "starpu_STATE_345");
}
void handleEZTRACE_starpu_starpu_execute_on_each_worker_692() {
    FUNC_NAME;
    DECLARE_THREAD_ID_STR(thread_id, CUR_INDEX, CUR_THREAD_ID);
    CHANGE() popState (CURRENT, "ST_Thread", thread_id);
}
void handleEZTRACE_starpu_starpu_execute_on_each_worker_ex_693() {
    FUNC_NAME;
    DECLARE_THREAD_ID_STR(thread_id, CUR_INDEX, CUR_THREAD_ID);
    CHANGE() pushState (CURRENT, "ST_Thread", thread_id, "starpu_STATE_346");
}
void handleEZTRACE_starpu_starpu_execute_on_each_worker_ex_694() {
    FUNC_NAME;
    DECLARE_THREAD_ID_STR(thread_id, CUR_INDEX, CUR_THREAD_ID);
    CHANGE() popState (CURRENT, "ST_Thread", thread_id);
}
void handleEZTRACE_starpu_starpu_execute_on_specific_workers_695() {
    FUNC_NAME;
    DECLARE_THREAD_ID_STR(thread_id, CUR_INDEX, CUR_THREAD_ID);
    CHANGE() pushState (CURRENT, "ST_Thread", thread_id, "starpu_STATE_347");
}
void handleEZTRACE_starpu_starpu_execute_on_specific_workers_696() {
    FUNC_NAME;
    DECLARE_THREAD_ID_STR(thread_id, CUR_INDEX, CUR_THREAD_ID);
    CHANGE() popState (CURRENT, "ST_Thread", thread_id);
}
void handleEZTRACE_starpu_starpu_data_cpy_697() {
    FUNC_NAME;
    DECLARE_THREAD_ID_STR(thread_id, CUR_INDEX, CUR_THREAD_ID);
    CHANGE() pushState (CURRENT, "ST_Thread", thread_id, "starpu_STATE_348");
}
void handleEZTRACE_starpu_starpu_data_cpy_698() {
    FUNC_NAME;
    DECLARE_THREAD_ID_STR(thread_id, CUR_INDEX, CUR_THREAD_ID);
    CHANGE() popState (CURRENT, "ST_Thread", thread_id);
}
