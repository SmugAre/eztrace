/*
 * Copyright (C) CNRS, INRIA, Université Bordeaux 1, Télécom SudParis
 * See COPYING in top-level directory.
 */

#include "eztrace.h"
#include "eztrace_sampling.h"

#include "starpu_ev_codes.h"
#include "starpu.h"
#ifdef USE_MPI
#include "starpu_mpi.h"
#endif
#include "starpu_opencl.h"

// Internals
void (*lib_starpu_driver_start_job) (void *args, void *j, void *codelet_start, int rank, int profiling);
void (*lib_starpu_driver_end_job) (void * args, void *j, void *perf_arch, void *codelet_end, int rank, int profiling);

// API
void (*libstarpu_cublas_init) ();
void (*libstarpu_cublas_shutdown) ();
int (*libstarpu_init) (struct starpu_conf* conf);
void (*libstarpu_pause) ();
void (*libstarpu_resume) ();
void (*libstarpu_shutdown) ();
void (*libstarpu_data_unregister) (starpu_data_handle_t handle);
void (*libstarpu_data_unregister_no_coherency) (starpu_data_handle_t handle);
void (*libstarpu_data_unregister_submit) (starpu_data_handle_t handle);
void (*libstarpu_data_invalidate) (starpu_data_handle_t handle);
void (*libstarpu_data_invalidate_submit) (starpu_data_handle_t handle);
int (*libstarpu_data_acquire) (starpu_data_handle_t handle, enum starpu_data_access_mode mode);
int (*libstarpu_data_acquire_on_node) (starpu_data_handle_t handle, int node, enum starpu_data_access_mode mode);
int (*libstarpu_data_acquire_cb) (starpu_data_handle_t handle, enum starpu_data_access_mode mode, void (* callback), void *arg);
int (*libstarpu_data_acquire_on_node_cb) (starpu_data_handle_t handle, int node, enum starpu_data_access_mode mode, void (* callback), void *arg);
void (*libstarpu_data_release) (starpu_data_handle_t handle);
void (*libstarpu_data_release_on_node) (starpu_data_handle_t handle, int node);
int (*libstarpu_data_request_allocation) (starpu_data_handle_t handle, unsigned node);
uintptr_t (*libstarpu_malloc_on_node) (unsigned dst_node, size_t size);
void (*libstarpu_free_on_node) (unsigned dst_node, uintptr_t addr, size_t size);
#ifdef USE_MPI
int (*libstarpu_mpi_isend) (starpu_data_handle_t data_handle, starpu_mpi_req* req, int dest, int mpi_tag, MPI_Comm comm);
int (*libstarpu_mpi_irecv) (starpu_data_handle_t data_handle, starpu_mpi_req* req, int source, int mpi_tag, MPI_Comm comm);
int (*libstarpu_mpi_send) (starpu_data_handle_t data_handle, int dest, int mpi_tag, MPI_Comm comm);
int (*libstarpu_mpi_recv) (starpu_data_handle_t data_handle, int source, int mpi_tag, MPI_Comm comm, MPI_Status* status);
int (*libstarpu_mpi_isend_detached) (starpu_data_handle_t data_handle, int dest, int mpi_tag, MPI_Comm comm, void (* callback)(void *arg), void *arg);
int (*libstarpu_mpi_irecv_detached) (starpu_data_handle_t data_handle, int source, int mpi_tag, MPI_Comm comm, void (* callback)(void *arg), void *arg);
int (*libstarpu_mpi_wait) (starpu_mpi_req* req, MPI_Status* status);
int (*libstarpu_mpi_test) (starpu_mpi_req* req, int* flag, MPI_Status* status);
int (*libstarpu_mpi_barrier) (MPI_Comm comm);
int (*libstarpu_mpi_init) (int* argc, char*** argv, int initialize_mpi);
int (*libstarpu_mpi_initialize) ();
int (*libstarpu_mpi_initialize_extended) (int* rank, int* world_size);
int (*libstarpu_mpi_shutdown) ();
void (*libstarpu_mpi_get_data_on_node) (MPI_Comm comm, starpu_data_handle_t data_handle, int node);
void (*libstarpu_mpi_get_data_on_node_detached) (MPI_Comm comm, starpu_data_handle_t data_handle, int node, void (* callback)(void *arg), void *arg);
void (*libstarpu_mpi_redux_data) (MPI_Comm comm, starpu_data_handle_t data_handle);
int (*libstarpu_mpi_scatter_detached) (starpu_data_handle_t* data_handles, int count, int root, MPI_Comm comm, void (* scallback)(void *arg), void *arg,void (*rcallback)(void *), void *rarg);
int (*libstarpu_mpi_gather_detached) (starpu_data_handle_t* data_handles, int count, int root, MPI_Comm comm, void (* scallback)(void *arg), void *arg, void (*rcallback)(void *), void *rarg);
int (*libstarpu_mpi_isend_detached_unlock_tag) (starpu_data_handle_t data_handle, int dest, int mpi_tag, MPI_Comm comm, starpu_tag_t tag);
int (*libstarpu_mpi_irecv_detached_unlock_tag) (starpu_data_handle_t data_handle, int source, int mpi_tag, MPI_Comm comm, starpu_tag_t tag);
int (*libstarpu_mpi_isend_array_detached_unlock_tag) (unsigned array_size, starpu_data_handle_t* data_handle, int* dest, int* mpi_tag, MPI_Comm* comm, starpu_tag_t tag);
int (*libstarpu_mpi_irecv_array_detached_unlock_tag) (unsigned array_size, starpu_data_handle_t* data_handle, int* source, int* mpi_tag, MPI_Comm* comm, starpu_tag_t tag);
void (*libstarpu_mpi_comm_amounts_retrieve) (size_t* comm_amounts);
void (*libstarpu_mpi_cache_flush) (MPI_Comm comm, starpu_data_handle_t data_handle);
void (*libstarpu_mpi_cache_flush_all_data) (MPI_Comm comm);
#endif USE_MPI
#ifndef HAVE_STARPU_MPI_DATA_REGISTER_COMM
void (*libstarpu_mpi_data_register) (starpu_data_handle_t data_handle, int tag, int rank);
#else
void (*libstarpu_mpi_data_register_comm) (starpu_data_handle_t data_handle, int tag, int rank, MPI_Comm comm);
#endif

void (*libstarpu_sched_ctx_add_workers) (int* workerids_ctx, int nworkers_ctx, unsigned sched_ctx_id);
void (*libstarpu_sched_ctx_remove_workers) (int* workerids_ctx, int nworkers_ctx, unsigned sched_ctx_id);
void (*libstarpu_sched_ctx_delete) (unsigned sched_ctx_id);
void (*libstarpu_sched_ctx_set_inheritor) (unsigned sched_ctx_id, unsigned inheritor);
void (*libstarpu_sched_ctx_set_context) (unsigned* sched_ctx_id);
unsigned (*libstarpu_sched_ctx_get_context) ();
void (*libstarpu_sched_ctx_stop_task_submission) ();
void (*libstarpu_sched_ctx_finished_submit) (unsigned sched_ctx_id);
struct starpu_worker_collection * (*libstarpu_sched_ctx_create_worker_collection) (unsigned sched_ctx_id, enum starpu_worker_collection_type type);
void (*libstarpu_sched_ctx_delete_worker_collection) (unsigned sched_ctx_id);
struct starpu_worker_collection * (*libstarpu_sched_ctx_get_worker_collection) (unsigned sched_ctx_id);
int (*libstarpu_prefetch_task_input_on_node) (struct starpu_task* task, unsigned node);
int (*libstarpu_malloc) (void** A, size_t dim);
int (*libstarpu_free) (void* A);
int (*libstarpu_malloc_flags) (void** A, size_t dim, int flags);
int (*libstarpu_free_flags) (void* A, size_t dim, int flags);
int (*libstarpu_tag_wait) (starpu_tag_t id);
int (*libstarpu_tag_wait_array) (unsigned ntags, starpu_tag_t* id);
void (*libstarpu_tag_notify_from_apps) (starpu_tag_t id);
void (*libstarpu_task_init) (struct starpu_task* task);
void (*libstarpu_task_clean) (struct starpu_task* task);
struct starpu_task * (*libstarpu_task_create) ();
void (*libstarpu_task_destroy) (struct starpu_task* task);
int (*libstarpu_task_submit) (struct starpu_task* task);
int (*libstarpu_task_submit_to_ctx) (struct starpu_task* task, unsigned sched_ctx_id);
int (*libstarpu_task_wait) (struct starpu_task* task);
int (*libstarpu_task_wait_for_all) ();
int (*libstarpu_task_wait_for_all_in_ctx) (unsigned sched_ctx_id);
int (*libstarpu_task_wait_for_no_ready) ();
int (*libstarpu_task_nready) ();
int (*libstarpu_task_nsubmitted) ();
struct starpu_task * (*libstarpu_task_dup) (struct starpu_task* task);
void (*libstarpu_task_bundle_create) (starpu_task_bundle_t* bundle);
int (*libstarpu_task_bundle_insert) (starpu_task_bundle_t bundle, struct starpu_task* task);
int (*libstarpu_task_bundle_remove) (starpu_task_bundle_t bundle, struct starpu_task* task);
void (*libstarpu_task_bundle_close) (starpu_task_bundle_t bundle);
void (*libstarpu_create_sync_task) (starpu_tag_t sync_tag, unsigned ndeps, starpu_tag_t* deps, void (* callback)(void *), void *callback_arg);
void (*libstarpu_execute_on_each_worker) (void (* func)(void *), void *arg, uint32_t where);
void (*libstarpu_execute_on_each_worker_ex) (void (* func)(void *), void *arg, uint32_t where, const char * name);
void (*libstarpu_execute_on_specific_workers) (void (* func)(void*), void * arg, unsigned num_workers, unsigned * workers, const char * name);
int (*libstarpu_data_cpy) (starpu_data_handle_t dst_handle, starpu_data_handle_t src_handle, int asynchronous, void (* callback_func)(void*), void *callback_arg);

void _starpu_driver_start_job (void *args, void *j, void *codelet_start, int rank, int profiling) {
    FUNCTION_ENTRY;
    EZTRACE_EVENT_PACKED_0(EZTRACE_starpu__starpu_driver_start_job);
    lib_starpu_driver_start_job (args, j, codelet_start, rank, profiling);
}

void _starpu_driver_end_job (void* args, void* j, void* perf_arch, void *codelet_end, int rank, int profiling) {
    FUNCTION_ENTRY;
    EZTRACE_EVENT_PACKED_0 (EZTRACE_starpu__starpu_driver_end_job);
    lib_starpu_driver_end_job (args, j, perf_arch, codelet_end, rank, profiling);
}


void starpu_cublas_init () {
  FUNCTION_ENTRY
	EZTRACE_EVENT_PACKED_0 (EZTRACE_starpu_starpu_cublas_init_41);
	libstarpu_cublas_init ();
	EZTRACE_EVENT_PACKED_0 (EZTRACE_starpu_starpu_cublas_init_42);
}

void starpu_cublas_shutdown () {
  FUNCTION_ENTRY
	EZTRACE_EVENT_PACKED_0 (EZTRACE_starpu_starpu_cublas_shutdown_43);
	libstarpu_cublas_shutdown ();
	EZTRACE_EVENT_PACKED_0 (EZTRACE_starpu_starpu_cublas_shutdown_44);
}

int starpu_init (struct starpu_conf* conf) {
  FUNCTION_ENTRY
	EZTRACE_EVENT_PACKED_1 (EZTRACE_starpu_starpu_init_47, conf);
	int ret = libstarpu_init (conf);
	EZTRACE_EVENT_PACKED_1 (EZTRACE_starpu_starpu_init_48, conf);
	return ret;
}

void starpu_pause () {
  FUNCTION_ENTRY
	EZTRACE_EVENT_PACKED_0 (EZTRACE_starpu_starpu_pause_49);
	libstarpu_pause ();
	EZTRACE_EVENT_PACKED_0 (EZTRACE_starpu_starpu_pause_50);
}

void starpu_resume () {
  FUNCTION_ENTRY
	EZTRACE_EVENT_PACKED_0 (EZTRACE_starpu_starpu_resume_51);
	libstarpu_resume ();
	EZTRACE_EVENT_PACKED_0 (EZTRACE_starpu_starpu_resume_52);
}

void starpu_shutdown () {
  FUNCTION_ENTRY
	EZTRACE_EVENT_PACKED_0 (EZTRACE_starpu_starpu_shutdown_53);
	libstarpu_shutdown ();
	EZTRACE_EVENT_PACKED_0 (EZTRACE_starpu_starpu_shutdown_54);
}

void starpu_data_unregister (starpu_data_handle_t handle) {
  FUNCTION_ENTRY
	EZTRACE_EVENT_PACKED_1 (EZTRACE_starpu_starpu_data_unregister_79, handle);
	libstarpu_data_unregister (handle);
	EZTRACE_EVENT_PACKED_1 (EZTRACE_starpu_starpu_data_unregister_80, handle);
}

void starpu_data_unregister_no_coherency (starpu_data_handle_t handle) {
  FUNCTION_ENTRY
	EZTRACE_EVENT_PACKED_1 (EZTRACE_starpu_starpu_data_unregister_no_coherency_81, handle);
	libstarpu_data_unregister_no_coherency (handle);
	EZTRACE_EVENT_PACKED_1 (EZTRACE_starpu_starpu_data_unregister_no_coherency_82, handle);
}

void starpu_data_unregister_submit (starpu_data_handle_t handle) {
  FUNCTION_ENTRY
	EZTRACE_EVENT_PACKED_1 (EZTRACE_starpu_starpu_data_unregister_submit_83, handle);
	libstarpu_data_unregister_submit (handle);
	EZTRACE_EVENT_PACKED_1 (EZTRACE_starpu_starpu_data_unregister_submit_84, handle);
}

void starpu_data_invalidate (starpu_data_handle_t handle) {
  FUNCTION_ENTRY
	EZTRACE_EVENT_PACKED_1 (EZTRACE_starpu_starpu_data_invalidate_85, handle);
	libstarpu_data_invalidate (handle);
	EZTRACE_EVENT_PACKED_1 (EZTRACE_starpu_starpu_data_invalidate_86, handle);
}

void starpu_data_invalidate_submit (starpu_data_handle_t handle) {
  FUNCTION_ENTRY
	EZTRACE_EVENT_PACKED_1 (EZTRACE_starpu_starpu_data_invalidate_submit_87, handle);
	libstarpu_data_invalidate_submit (handle);
	EZTRACE_EVENT_PACKED_1 (EZTRACE_starpu_starpu_data_invalidate_submit_88, handle);
}

int starpu_data_acquire (starpu_data_handle_t handle, enum starpu_data_access_mode mode) {
  FUNCTION_ENTRY
	EZTRACE_EVENT_PACKED_2 (EZTRACE_starpu_starpu_data_acquire_91, handle, mode);
	int ret = libstarpu_data_acquire (handle, mode);
	EZTRACE_EVENT_PACKED_2 (EZTRACE_starpu_starpu_data_acquire_92, handle, mode);
	return ret;
}

int starpu_data_acquire_on_node (starpu_data_handle_t handle, int node, enum starpu_data_access_mode mode) {
  FUNCTION_ENTRY
	EZTRACE_EVENT_PACKED_3 (EZTRACE_starpu_starpu_data_acquire_on_node_93, handle, node, mode);
	int ret = libstarpu_data_acquire_on_node (handle, node, mode);
	EZTRACE_EVENT_PACKED_3 (EZTRACE_starpu_starpu_data_acquire_on_node_94, handle, node, mode);
	return ret;
}

int starpu_data_acquire_cb (starpu_data_handle_t handle, enum starpu_data_access_mode mode, void (* callback)(void *), void *arg) {
  FUNCTION_ENTRY
      EZTRACE_EVENT_PACKED_4 (EZTRACE_starpu_starpu_data_acquire_cb_95, handle, mode, callback, arg);
  int ret = libstarpu_data_acquire_cb (handle, mode, callback, arg);
	EZTRACE_EVENT_PACKED_4 (EZTRACE_starpu_starpu_data_acquire_cb_96, handle, mode, callback, arg);
	return ret;
}

int starpu_data_acquire_on_node_cb (starpu_data_handle_t handle, int node, enum starpu_data_access_mode mode, void (* callback)(void *), void *arg) {
  FUNCTION_ENTRY
      EZTRACE_EVENT_PACKED_5(EZTRACE_starpu_starpu_data_acquire_on_node_cb_97, handle, node, mode, callback, arg);
  int ret = libstarpu_data_acquire_on_node_cb (handle, node, mode, callback, arg);
	EZTRACE_EVENT_PACKED_5(EZTRACE_starpu_starpu_data_acquire_on_node_cb_98, handle, node, mode, callback, arg);
	return ret;
}

void starpu_data_release (starpu_data_handle_t handle) {
  FUNCTION_ENTRY
	EZTRACE_EVENT_PACKED_1 (EZTRACE_starpu_starpu_data_release_99, handle);
	libstarpu_data_release (handle);
	EZTRACE_EVENT_PACKED_1 (EZTRACE_starpu_starpu_data_release_100, handle);
}

void starpu_data_release_on_node (starpu_data_handle_t handle, int node) {
  FUNCTION_ENTRY
	EZTRACE_EVENT_PACKED_2 (EZTRACE_starpu_starpu_data_release_on_node_101, handle, node);
	libstarpu_data_release_on_node (handle, node);
	EZTRACE_EVENT_PACKED_2 (EZTRACE_starpu_starpu_data_release_on_node_102, handle, node);
}

int starpu_data_request_allocation (starpu_data_handle_t handle, unsigned node) {
  FUNCTION_ENTRY
	EZTRACE_EVENT_PACKED_2 (EZTRACE_starpu_starpu_data_request_allocation_105, handle, node);
	int ret = libstarpu_data_request_allocation (handle, node);
	EZTRACE_EVENT_PACKED_2 (EZTRACE_starpu_starpu_data_request_allocation_106, handle, node);
	return ret;
}

uintptr_t starpu_malloc_on_node (unsigned dst_node, size_t size) {
  FUNCTION_ENTRY
	EZTRACE_EVENT_PACKED_2 (EZTRACE_starpu_starpu_malloc_on_node_183, dst_node, size);
	uintptr_t ret = libstarpu_malloc_on_node (dst_node, size);
	EZTRACE_EVENT_PACKED_2 (EZTRACE_starpu_starpu_malloc_on_node_184, dst_node, size);
	return ret;
}

void starpu_free_on_node (unsigned dst_node, uintptr_t addr, size_t size) {
  FUNCTION_ENTRY
	EZTRACE_EVENT_PACKED_3 (EZTRACE_starpu_starpu_free_on_node_185, dst_node, addr, size);
	libstarpu_free_on_node (dst_node, addr, size);
	EZTRACE_EVENT_PACKED_3 (EZTRACE_starpu_starpu_free_on_node_186, dst_node, addr, size);
}

#ifdef USE_MPI

int starpu_mpi_isend (starpu_data_handle_t data_handle, starpu_mpi_req* req, int dest, int mpi_tag, MPI_Comm comm) {
  FUNCTION_ENTRY
	EZTRACE_EVENT_PACKED_4 (EZTRACE_starpu_starpu_mpi_isend_311, data_handle, req, dest, mpi_tag);
	int ret = libstarpu_mpi_isend (data_handle, req, dest, mpi_tag, comm);
	EZTRACE_EVENT_PACKED_4 (EZTRACE_starpu_starpu_mpi_isend_312, data_handle, req, dest, mpi_tag);
	return ret;
}

int starpu_mpi_irecv (starpu_data_handle_t data_handle, starpu_mpi_req* req, int source, int mpi_tag, MPI_Comm comm) {
  FUNCTION_ENTRY
	EZTRACE_EVENT_PACKED_4 (EZTRACE_starpu_starpu_mpi_irecv_313, data_handle, req, source, mpi_tag);
	int ret = libstarpu_mpi_irecv (data_handle, req, source, mpi_tag, comm);
	EZTRACE_EVENT_PACKED_4 (EZTRACE_starpu_starpu_mpi_irecv_314, data_handle, req, source, mpi_tag);
	return ret;
}

int starpu_mpi_send (starpu_data_handle_t data_handle, int dest, int mpi_tag, MPI_Comm comm) {
  FUNCTION_ENTRY
	EZTRACE_EVENT_PACKED_4 (EZTRACE_starpu_starpu_mpi_send_315, data_handle, dest, mpi_tag, comm);
	int ret = libstarpu_mpi_send (data_handle, dest, mpi_tag, comm);
	EZTRACE_EVENT_PACKED_4 (EZTRACE_starpu_starpu_mpi_send_316, data_handle, dest, mpi_tag, comm);
	return ret;
}

int starpu_mpi_recv (starpu_data_handle_t data_handle, int source, int mpi_tag, MPI_Comm comm, MPI_Status* status) {
  FUNCTION_ENTRY
	EZTRACE_EVENT_PACKED_4 (EZTRACE_starpu_starpu_mpi_recv_317, data_handle, source, mpi_tag, comm);
	int ret = libstarpu_mpi_recv (data_handle, source, mpi_tag, comm, status);
	EZTRACE_EVENT_PACKED_4 (EZTRACE_starpu_starpu_mpi_recv_318, data_handle, source, mpi_tag, comm);
	return ret;
}

int starpu_mpi_isend_detached (starpu_data_handle_t data_handle, int dest, int mpi_tag, MPI_Comm comm, void (* callback)(void *arg), void *arg) {
  FUNCTION_ENTRY
	EZTRACE_EVENT_PACKED_4 (EZTRACE_starpu_starpu_mpi_isend_detached_319, data_handle, dest, mpi_tag, comm);
  int ret = libstarpu_mpi_isend_detached (data_handle, dest, mpi_tag, comm, callback, arg);
	EZTRACE_EVENT_PACKED_4 (EZTRACE_starpu_starpu_mpi_isend_detached_320, data_handle, dest, mpi_tag, comm);
	return ret;
}

int starpu_mpi_irecv_detached (starpu_data_handle_t data_handle, int source, int mpi_tag, MPI_Comm comm, void (* callback)(void *arg), void *arg) {
  FUNCTION_ENTRY
	EZTRACE_EVENT_PACKED_4 (EZTRACE_starpu_starpu_mpi_irecv_detached_321, data_handle, source, mpi_tag, comm);
  int ret = libstarpu_mpi_irecv_detached (data_handle, source, mpi_tag, comm, callback, arg);
	EZTRACE_EVENT_PACKED_4 (EZTRACE_starpu_starpu_mpi_irecv_detached_322, data_handle, source, mpi_tag, comm);
	return ret;
}

int starpu_mpi_wait (starpu_mpi_req* req, MPI_Status* status) {
  FUNCTION_ENTRY
	EZTRACE_EVENT_PACKED_2 (EZTRACE_starpu_starpu_mpi_wait_323, req, status);
	int ret = libstarpu_mpi_wait (req, status);
	EZTRACE_EVENT_PACKED_2 (EZTRACE_starpu_starpu_mpi_wait_324, req, status);
	return ret;
}

int starpu_mpi_test (starpu_mpi_req* req, int* flag, MPI_Status* status) {
  FUNCTION_ENTRY
	EZTRACE_EVENT_PACKED_3 (EZTRACE_starpu_starpu_mpi_test_325, req, flag, status);
	int ret = libstarpu_mpi_test (req, flag, status);
	EZTRACE_EVENT_PACKED_3 (EZTRACE_starpu_starpu_mpi_test_326, req, flag, status);
	return ret;
}

int starpu_mpi_barrier (MPI_Comm comm) {
  FUNCTION_ENTRY
	EZTRACE_EVENT_PACKED_1 (EZTRACE_starpu_starpu_mpi_barrier_327, comm);
	int ret = libstarpu_mpi_barrier (comm);
	EZTRACE_EVENT_PACKED_1 (EZTRACE_starpu_starpu_mpi_barrier_328, comm);
	return ret;
}

int starpu_mpi_init (int* argc, char*** argv, int initialize_mpi) {
  FUNCTION_ENTRY
	EZTRACE_EVENT_PACKED_3 (EZTRACE_starpu_starpu_mpi_init_329, argc, argv, initialize_mpi);
	int ret = libstarpu_mpi_init (argc, argv, initialize_mpi);
	EZTRACE_EVENT_PACKED_3 (EZTRACE_starpu_starpu_mpi_init_330, argc, argv, initialize_mpi);
	return ret;
}

int starpu_mpi_initialize () {
  FUNCTION_ENTRY
	EZTRACE_EVENT_PACKED_0 (EZTRACE_starpu_starpu_mpi_initialize_331);
	int ret = libstarpu_mpi_initialize ();
	EZTRACE_EVENT_PACKED_0 (EZTRACE_starpu_starpu_mpi_initialize_332);
	return ret;
}

int starpu_mpi_initialize_extended (int* rank, int* world_size) {
  FUNCTION_ENTRY
	EZTRACE_EVENT_PACKED_2 (EZTRACE_starpu_starpu_mpi_initialize_extended_333, rank, world_size);
	int ret = libstarpu_mpi_initialize_extended (rank, world_size);
	EZTRACE_EVENT_PACKED_2 (EZTRACE_starpu_starpu_mpi_initialize_extended_334, rank, world_size);
	return ret;
}

int starpu_mpi_shutdown () {
  FUNCTION_ENTRY
	EZTRACE_EVENT_PACKED_0 (EZTRACE_starpu_starpu_mpi_shutdown_335);
	int ret = libstarpu_mpi_shutdown ();
	EZTRACE_EVENT_PACKED_0 (EZTRACE_starpu_starpu_mpi_shutdown_336);
	return ret;
}

void starpu_mpi_get_data_on_node (MPI_Comm comm, starpu_data_handle_t data_handle, int node) {
  FUNCTION_ENTRY
	EZTRACE_EVENT_PACKED_3 (EZTRACE_starpu_starpu_mpi_get_data_on_node_337, comm, data_handle, node);
	libstarpu_mpi_get_data_on_node (comm, data_handle, node);
	EZTRACE_EVENT_PACKED_3 (EZTRACE_starpu_starpu_mpi_get_data_on_node_338, comm, data_handle, node);
}

void starpu_mpi_get_data_on_node_detached (MPI_Comm comm, starpu_data_handle_t data_handle, int node, void (* callback)(void *arg), void *arg) {
  FUNCTION_ENTRY
	EZTRACE_EVENT_PACKED_3 (EZTRACE_starpu_starpu_mpi_get_data_on_node_detached_339, comm, data_handle, node);
  libstarpu_mpi_get_data_on_node_detached (comm, data_handle, node, callback, arg);
	EZTRACE_EVENT_PACKED_3 (EZTRACE_starpu_starpu_mpi_get_data_on_node_detached_340, comm, data_handle, node);
}

void starpu_mpi_redux_data (MPI_Comm comm, starpu_data_handle_t data_handle) {
  FUNCTION_ENTRY
	EZTRACE_EVENT_PACKED_2 (EZTRACE_starpu_starpu_mpi_redux_data_341, comm, data_handle);
	libstarpu_mpi_redux_data (comm, data_handle);
	EZTRACE_EVENT_PACKED_2 (EZTRACE_starpu_starpu_mpi_redux_data_342, comm, data_handle);
}

int starpu_mpi_scatter_detached (starpu_data_handle_t* data_handles, int count, int root, MPI_Comm comm, void (* scallback)(void *arg), void *arg, void (*rcallback)(void *), void *rarg) {
  FUNCTION_ENTRY
	EZTRACE_EVENT_PACKED_4 (EZTRACE_starpu_starpu_mpi_scatter_detached_343, data_handles, count, root, comm);
  int ret = libstarpu_mpi_scatter_detached (data_handles, count, root, comm, scallback, arg, rcallback, rarg);
	EZTRACE_EVENT_PACKED_4 (EZTRACE_starpu_starpu_mpi_scatter_detached_344, data_handles, count, root, comm);
	return ret;
}

int starpu_mpi_gather_detached (starpu_data_handle_t* data_handles, int count, int root, MPI_Comm comm, void (* scallback)(void *arg), void *arg, void (*rcallback)(void *), void *rarg) {
  FUNCTION_ENTRY
	EZTRACE_EVENT_PACKED_4 (EZTRACE_starpu_starpu_mpi_gather_detached_345, data_handles, count, root, comm);
  int ret = libstarpu_mpi_gather_detached (data_handles, count, root, comm, scallback, arg, rcallback, rarg);
	EZTRACE_EVENT_PACKED_4 (EZTRACE_starpu_starpu_mpi_gather_detached_346, data_handles, count, root, comm);
	return ret;
}

int starpu_mpi_isend_detached_unlock_tag (starpu_data_handle_t data_handle, int dest, int mpi_tag, MPI_Comm comm, starpu_tag_t tag) {
  FUNCTION_ENTRY
	EZTRACE_EVENT_PACKED_4 (EZTRACE_starpu_starpu_mpi_isend_detached_unlock_tag_347, data_handle, dest, mpi_tag, comm);
	int ret = libstarpu_mpi_isend_detached_unlock_tag (data_handle, dest, mpi_tag, comm, tag);
	EZTRACE_EVENT_PACKED_4 (EZTRACE_starpu_starpu_mpi_isend_detached_unlock_tag_348, data_handle, dest, mpi_tag, comm);
	return ret;
}

int starpu_mpi_irecv_detached_unlock_tag (starpu_data_handle_t data_handle, int source, int mpi_tag, MPI_Comm comm, starpu_tag_t tag) {
  FUNCTION_ENTRY
	EZTRACE_EVENT_PACKED_4 (EZTRACE_starpu_starpu_mpi_irecv_detached_unlock_tag_349, data_handle, source, mpi_tag, comm);
	int ret = libstarpu_mpi_irecv_detached_unlock_tag (data_handle, source, mpi_tag, comm, tag);
	EZTRACE_EVENT_PACKED_4 (EZTRACE_starpu_starpu_mpi_irecv_detached_unlock_tag_350, data_handle, source, mpi_tag, comm);
	return ret;
}

int starpu_mpi_isend_array_detached_unlock_tag (unsigned array_size, starpu_data_handle_t* data_handle, int* dest, int* mpi_tag, MPI_Comm* comm, starpu_tag_t tag) {
  FUNCTION_ENTRY
	EZTRACE_EVENT_PACKED_4 (EZTRACE_starpu_starpu_mpi_isend_array_detached_unlock_tag_351, array_size, data_handle, dest, mpi_tag);
	int ret = libstarpu_mpi_isend_array_detached_unlock_tag (array_size, data_handle, dest, mpi_tag, comm, tag);
	EZTRACE_EVENT_PACKED_4 (EZTRACE_starpu_starpu_mpi_isend_array_detached_unlock_tag_352, array_size, data_handle, dest, mpi_tag);
	return ret;
}

int starpu_mpi_irecv_array_detached_unlock_tag (unsigned array_size, starpu_data_handle_t* data_handle, int* source, int* mpi_tag, MPI_Comm* comm, starpu_tag_t tag) {
  FUNCTION_ENTRY
	EZTRACE_EVENT_PACKED_4 (EZTRACE_starpu_starpu_mpi_irecv_array_detached_unlock_tag_353, array_size, data_handle, source, mpi_tag);
	int ret = libstarpu_mpi_irecv_array_detached_unlock_tag (array_size, data_handle, source, mpi_tag, comm, tag);
	EZTRACE_EVENT_PACKED_4 (EZTRACE_starpu_starpu_mpi_irecv_array_detached_unlock_tag_354, array_size, data_handle, source, mpi_tag);
	return ret;
}

void starpu_mpi_comm_amounts_retrieve (size_t* comm_amounts) {
  FUNCTION_ENTRY
	EZTRACE_EVENT_PACKED_1 (EZTRACE_starpu_starpu_mpi_comm_amounts_retrieve_355, comm_amounts);
	libstarpu_mpi_comm_amounts_retrieve (comm_amounts);
	EZTRACE_EVENT_PACKED_1 (EZTRACE_starpu_starpu_mpi_comm_amounts_retrieve_356, comm_amounts);
}

void starpu_mpi_cache_flush (MPI_Comm comm, starpu_data_handle_t data_handle) {
  FUNCTION_ENTRY
	EZTRACE_EVENT_PACKED_2 (EZTRACE_starpu_starpu_mpi_cache_flush_357, comm, data_handle);
	libstarpu_mpi_cache_flush (comm, data_handle);
	EZTRACE_EVENT_PACKED_2 (EZTRACE_starpu_starpu_mpi_cache_flush_358, comm, data_handle);
}

void starpu_mpi_cache_flush_all_data (MPI_Comm comm) {
  FUNCTION_ENTRY
	EZTRACE_EVENT_PACKED_1 (EZTRACE_starpu_starpu_mpi_cache_flush_all_data_359, comm);
	libstarpu_mpi_cache_flush_all_data (comm);
	EZTRACE_EVENT_PACKED_1 (EZTRACE_starpu_starpu_mpi_cache_flush_all_data_360, comm);
}
#endif
#ifndef HAVE_STARPU_MPI_DATA_REGISTER_COMM
void starpu_mpi_data_register (starpu_data_handle_t data_handle, int tag, int rank) {
    FUNCTION_ENTRY
    EZTRACE_EVENT_PACKED_0 (EZTRACE_starpu_starpu_mpi_data_register_361);
	libstarpu_mpi_data_register (data_handle, tag, rank);
	EZTRACE_EVENT_PACKED_0 (EZTRACE_starpu_starpu_mpi_data_register_362);
}
#else
void starpu_mpi_data_register_comm(starpu_data_handle_t data_handle, int tag, int rank, MPI_Comm comm) {
    FUNCTION_ENTRY
    EZTRACE_EVENT_PACKED_0 (EZTRACE_starpu_starpu_mpi_data_register_361);
    libstarpu_mpi_data_register_comm (data_handle, tag, rank, comm);
    EZTRACE_EVENT_PACKED_0 (EZTRACE_starpu_starpu_mpi_data_register_362);
}
#endif



void starpu_sched_ctx_add_workers (int* workerids_ctx, int nworkers_ctx, unsigned sched_ctx_id) {
  FUNCTION_ENTRY
	EZTRACE_EVENT_PACKED_3 (EZTRACE_starpu_starpu_sched_ctx_add_workers_411, workerids_ctx, nworkers_ctx, sched_ctx_id);
	libstarpu_sched_ctx_add_workers (workerids_ctx, nworkers_ctx, sched_ctx_id);
	EZTRACE_EVENT_PACKED_3 (EZTRACE_starpu_starpu_sched_ctx_add_workers_412, workerids_ctx, nworkers_ctx, sched_ctx_id);
}

void starpu_sched_ctx_remove_workers (int* workerids_ctx, int nworkers_ctx, unsigned sched_ctx_id) {
  FUNCTION_ENTRY
	EZTRACE_EVENT_PACKED_3 (EZTRACE_starpu_starpu_sched_ctx_remove_workers_413, workerids_ctx, nworkers_ctx, sched_ctx_id);
	libstarpu_sched_ctx_remove_workers (workerids_ctx, nworkers_ctx, sched_ctx_id);
	EZTRACE_EVENT_PACKED_3 (EZTRACE_starpu_starpu_sched_ctx_remove_workers_414, workerids_ctx, nworkers_ctx, sched_ctx_id);
}

void starpu_sched_ctx_delete (unsigned sched_ctx_id) {
  FUNCTION_ENTRY
	EZTRACE_EVENT_PACKED_1 (EZTRACE_starpu_starpu_sched_ctx_delete_415, sched_ctx_id);
	libstarpu_sched_ctx_delete (sched_ctx_id);
	EZTRACE_EVENT_PACKED_1 (EZTRACE_starpu_starpu_sched_ctx_delete_416, sched_ctx_id);
}

void starpu_sched_ctx_set_inheritor (unsigned sched_ctx_id, unsigned inheritor) {
  FUNCTION_ENTRY
	EZTRACE_EVENT_PACKED_2 (EZTRACE_starpu_starpu_sched_ctx_set_inheritor_417, sched_ctx_id, inheritor);
	libstarpu_sched_ctx_set_inheritor (sched_ctx_id, inheritor);
	EZTRACE_EVENT_PACKED_2 (EZTRACE_starpu_starpu_sched_ctx_set_inheritor_418, sched_ctx_id, inheritor);
}

void starpu_sched_ctx_set_context (unsigned* sched_ctx_id) {
  FUNCTION_ENTRY
	EZTRACE_EVENT_PACKED_1 (EZTRACE_starpu_starpu_sched_ctx_set_context_419, sched_ctx_id);
	libstarpu_sched_ctx_set_context (sched_ctx_id);
	EZTRACE_EVENT_PACKED_1 (EZTRACE_starpu_starpu_sched_ctx_set_context_420, sched_ctx_id);
}

unsigned starpu_sched_ctx_get_context () {
  FUNCTION_ENTRY
	EZTRACE_EVENT_PACKED_0 (EZTRACE_starpu_starpu_sched_ctx_get_context_421);
	unsigned ret = libstarpu_sched_ctx_get_context ();
	EZTRACE_EVENT_PACKED_0 (EZTRACE_starpu_starpu_sched_ctx_get_context_422);
	return ret;
}

void starpu_sched_ctx_stop_task_submission () {
  FUNCTION_ENTRY
	EZTRACE_EVENT_PACKED_0 (EZTRACE_starpu_starpu_sched_ctx_stop_task_submission_423);
	libstarpu_sched_ctx_stop_task_submission ();
	EZTRACE_EVENT_PACKED_0 (EZTRACE_starpu_starpu_sched_ctx_stop_task_submission_424);
}

void starpu_sched_ctx_finished_submit (unsigned sched_ctx_id) {
  FUNCTION_ENTRY
	EZTRACE_EVENT_PACKED_1 (EZTRACE_starpu_starpu_sched_ctx_finished_submit_425, sched_ctx_id);
	libstarpu_sched_ctx_finished_submit (sched_ctx_id);
	EZTRACE_EVENT_PACKED_1 (EZTRACE_starpu_starpu_sched_ctx_finished_submit_426, sched_ctx_id);
}

struct starpu_worker_collection * starpu_sched_ctx_create_worker_collection (unsigned sched_ctx_id, enum starpu_worker_collection_type type) {
  FUNCTION_ENTRY
	EZTRACE_EVENT_PACKED_2 (EZTRACE_starpu_starpu_sched_ctx_create_worker_collection_461, sched_ctx_id, type);
	struct starpu_worker_collection * ret = libstarpu_sched_ctx_create_worker_collection (sched_ctx_id, type);
	EZTRACE_EVENT_PACKED_2 (EZTRACE_starpu_starpu_sched_ctx_create_worker_collection_462, sched_ctx_id, type);
	return ret;
}

void starpu_sched_ctx_delete_worker_collection (unsigned sched_ctx_id) {
  FUNCTION_ENTRY
	EZTRACE_EVENT_PACKED_1 (EZTRACE_starpu_starpu_sched_ctx_delete_worker_collection_463, sched_ctx_id);
	libstarpu_sched_ctx_delete_worker_collection (sched_ctx_id);
	EZTRACE_EVENT_PACKED_1 (EZTRACE_starpu_starpu_sched_ctx_delete_worker_collection_464, sched_ctx_id);
}

struct starpu_worker_collection * starpu_sched_ctx_get_worker_collection (unsigned sched_ctx_id) {
  FUNCTION_ENTRY
	EZTRACE_EVENT_PACKED_1 (EZTRACE_starpu_starpu_sched_ctx_get_worker_collection_465, sched_ctx_id);
	struct starpu_worker_collection * ret = libstarpu_sched_ctx_get_worker_collection (sched_ctx_id);
	EZTRACE_EVENT_PACKED_1 (EZTRACE_starpu_starpu_sched_ctx_get_worker_collection_466, sched_ctx_id);
	return ret;
}

int starpu_prefetch_task_input_on_node (struct starpu_task* task, unsigned node) {
  FUNCTION_ENTRY
	EZTRACE_EVENT_PACKED_2 (EZTRACE_starpu_starpu_prefetch_task_input_on_node_495, task, node);
	int ret = libstarpu_prefetch_task_input_on_node (task, node);
	EZTRACE_EVENT_PACKED_2 (EZTRACE_starpu_starpu_prefetch_task_input_on_node_496, task, node);
	return ret;
}

int starpu_malloc (void** A, size_t dim) {
  FUNCTION_ENTRY
	EZTRACE_EVENT_PACKED_2 (EZTRACE_starpu_starpu_malloc_521, A, dim);
	int ret = libstarpu_malloc (A, dim);
	EZTRACE_EVENT_PACKED_2 (EZTRACE_starpu_starpu_malloc_522, A, dim);
	return ret;
}

int starpu_free (void* A) {
  FUNCTION_ENTRY
	EZTRACE_EVENT_PACKED_1 (EZTRACE_starpu_starpu_free_523, A);
	int ret = libstarpu_free (A);
	EZTRACE_EVENT_PACKED_1 (EZTRACE_starpu_starpu_free_524, A);
	return ret;
}

int starpu_malloc_flags (void** A, size_t dim, int flags) {
  FUNCTION_ENTRY
	EZTRACE_EVENT_PACKED_3 (EZTRACE_starpu_starpu_malloc_flags_525, A, dim, flags);
	int ret = libstarpu_malloc_flags (A, dim, flags);
	EZTRACE_EVENT_PACKED_3 (EZTRACE_starpu_starpu_malloc_flags_526, A, dim, flags);
	return ret;
}

int starpu_free_flags (void* A, size_t dim, int flags) {
  FUNCTION_ENTRY
	EZTRACE_EVENT_PACKED_3 (EZTRACE_starpu_starpu_free_flags_527, A, dim, flags);
	int ret = libstarpu_free_flags (A, dim, flags);
	EZTRACE_EVENT_PACKED_3 (EZTRACE_starpu_starpu_free_flags_528, A, dim, flags);
	return ret;
}

int starpu_tag_wait (starpu_tag_t id) {
  FUNCTION_ENTRY
	EZTRACE_EVENT_PACKED_1 (EZTRACE_starpu_starpu_tag_wait_537, id);
	int ret = libstarpu_tag_wait (id);
	EZTRACE_EVENT_PACKED_1 (EZTRACE_starpu_starpu_tag_wait_538, id);
	return ret;
}

int starpu_tag_wait_array (unsigned ntags, starpu_tag_t* id) {
  FUNCTION_ENTRY
	EZTRACE_EVENT_PACKED_2 (EZTRACE_starpu_starpu_tag_wait_array_539, ntags, id);
	int ret = libstarpu_tag_wait_array (ntags, id);
	EZTRACE_EVENT_PACKED_2 (EZTRACE_starpu_starpu_tag_wait_array_540, ntags, id);
	return ret;
}

void starpu_tag_notify_from_apps (starpu_tag_t id) {
  FUNCTION_ENTRY
	EZTRACE_EVENT_PACKED_1 (EZTRACE_starpu_starpu_tag_notify_from_apps_541, id);
	libstarpu_tag_notify_from_apps (id);
	EZTRACE_EVENT_PACKED_1 (EZTRACE_starpu_starpu_tag_notify_from_apps_542, id);
}

void starpu_task_init (struct starpu_task* task) {
  FUNCTION_ENTRY
	EZTRACE_EVENT_PACKED_1 (EZTRACE_starpu_starpu_task_init_547, task);
	libstarpu_task_init (task);
	EZTRACE_EVENT_PACKED_1 (EZTRACE_starpu_starpu_task_init_548, task);
}

void starpu_task_clean (struct starpu_task* task) {
  FUNCTION_ENTRY
	EZTRACE_EVENT_PACKED_1 (EZTRACE_starpu_starpu_task_clean_549, task);
	libstarpu_task_clean (task);
	EZTRACE_EVENT_PACKED_1 (EZTRACE_starpu_starpu_task_clean_550, task);
}

struct starpu_task * starpu_task_create () {
  FUNCTION_ENTRY
	EZTRACE_EVENT_PACKED_0 (EZTRACE_starpu_starpu_task_create_551);
	struct starpu_task * ret = libstarpu_task_create ();
	EZTRACE_EVENT_PACKED_0 (EZTRACE_starpu_starpu_task_create_552);
	return ret;
}

void starpu_task_destroy (struct starpu_task* task) {
  FUNCTION_ENTRY
	EZTRACE_EVENT_PACKED_1 (EZTRACE_starpu_starpu_task_destroy_553, task);
	libstarpu_task_destroy (task);
	EZTRACE_EVENT_PACKED_1 (EZTRACE_starpu_starpu_task_destroy_554, task);
}

int starpu_task_submit (struct starpu_task* task) {
  FUNCTION_ENTRY
	EZTRACE_EVENT_PACKED_1 (EZTRACE_starpu_starpu_task_submit_555, task);
	int ret = libstarpu_task_submit (task);
	EZTRACE_EVENT_PACKED_1 (EZTRACE_starpu_starpu_task_submit_556, task);
	return ret;
}

int starpu_task_submit_to_ctx (struct starpu_task* task, unsigned sched_ctx_id) {
  FUNCTION_ENTRY
	EZTRACE_EVENT_PACKED_2 (EZTRACE_starpu_starpu_task_submit_to_ctx_557, task, sched_ctx_id);
	int ret = libstarpu_task_submit_to_ctx (task, sched_ctx_id);
	EZTRACE_EVENT_PACKED_2 (EZTRACE_starpu_starpu_task_submit_to_ctx_558, task, sched_ctx_id);
	return ret;
}

int starpu_task_wait (struct starpu_task* task) {
  FUNCTION_ENTRY
	EZTRACE_EVENT_PACKED_1 (EZTRACE_starpu_starpu_task_wait_559, task);
	int ret = libstarpu_task_wait (task);
	EZTRACE_EVENT_PACKED_1 (EZTRACE_starpu_starpu_task_wait_560, task);
	return ret;
}

int starpu_task_wait_for_all () {
  FUNCTION_ENTRY
	EZTRACE_EVENT_PACKED_0 (EZTRACE_starpu_starpu_task_wait_for_all_561);
	int ret = libstarpu_task_wait_for_all ();
	EZTRACE_EVENT_PACKED_0 (EZTRACE_starpu_starpu_task_wait_for_all_562);
	return ret;
}

int starpu_task_wait_for_all_in_ctx (unsigned sched_ctx_id) {
  FUNCTION_ENTRY
	EZTRACE_EVENT_PACKED_1 (EZTRACE_starpu_starpu_task_wait_for_all_in_ctx_563, sched_ctx_id);
	int ret = libstarpu_task_wait_for_all_in_ctx (sched_ctx_id);
	EZTRACE_EVENT_PACKED_1 (EZTRACE_starpu_starpu_task_wait_for_all_in_ctx_564, sched_ctx_id);
	return ret;
}

int starpu_task_wait_for_no_ready () {
  FUNCTION_ENTRY
	EZTRACE_EVENT_PACKED_0 (EZTRACE_starpu_starpu_task_wait_for_no_ready_565);
	int ret = libstarpu_task_wait_for_no_ready ();
	EZTRACE_EVENT_PACKED_0 (EZTRACE_starpu_starpu_task_wait_for_no_ready_566);
	return ret;
}

int starpu_task_nready () {
  FUNCTION_ENTRY
	EZTRACE_EVENT_PACKED_0 (EZTRACE_starpu_starpu_task_nready_567);
	int ret = libstarpu_task_nready ();
	EZTRACE_EVENT_PACKED_0 (EZTRACE_starpu_starpu_task_nready_568);
	return ret;
}

int starpu_task_nsubmitted () {
  FUNCTION_ENTRY
	EZTRACE_EVENT_PACKED_0 (EZTRACE_starpu_starpu_task_nsubmitted_569);
	int ret = libstarpu_task_nsubmitted ();
	EZTRACE_EVENT_PACKED_0 (EZTRACE_starpu_starpu_task_nsubmitted_570);
	return ret;
}

struct starpu_task * starpu_task_dup (struct starpu_task* task) {
  FUNCTION_ENTRY
	EZTRACE_EVENT_PACKED_1 (EZTRACE_starpu_starpu_task_dup_581, task);
	struct starpu_task * ret = libstarpu_task_dup (task);
	EZTRACE_EVENT_PACKED_1 (EZTRACE_starpu_starpu_task_dup_582, task);
	return ret;
}

void starpu_task_bundle_create (starpu_task_bundle_t* bundle) {
  FUNCTION_ENTRY
	EZTRACE_EVENT_PACKED_1 (EZTRACE_starpu_starpu_task_bundle_create_587, bundle);
	libstarpu_task_bundle_create (bundle);
	EZTRACE_EVENT_PACKED_1 (EZTRACE_starpu_starpu_task_bundle_create_588, bundle);
}

int starpu_task_bundle_insert (starpu_task_bundle_t bundle, struct starpu_task* task) {
  FUNCTION_ENTRY
	EZTRACE_EVENT_PACKED_2 (EZTRACE_starpu_starpu_task_bundle_insert_589, bundle, task);
	int ret = libstarpu_task_bundle_insert (bundle, task);
	EZTRACE_EVENT_PACKED_2 (EZTRACE_starpu_starpu_task_bundle_insert_590, bundle, task);
	return ret;
}

int starpu_task_bundle_remove (starpu_task_bundle_t bundle, struct starpu_task* task) {
  FUNCTION_ENTRY
	EZTRACE_EVENT_PACKED_2 (EZTRACE_starpu_starpu_task_bundle_remove_591, bundle, task);
	int ret = libstarpu_task_bundle_remove (bundle, task);
	EZTRACE_EVENT_PACKED_2 (EZTRACE_starpu_starpu_task_bundle_remove_592, bundle, task);
	return ret;
}

void starpu_task_bundle_close (starpu_task_bundle_t bundle) {
  FUNCTION_ENTRY
	EZTRACE_EVENT_PACKED_1 (EZTRACE_starpu_starpu_task_bundle_close_593, bundle);
	libstarpu_task_bundle_close (bundle);
	EZTRACE_EVENT_PACKED_1 (EZTRACE_starpu_starpu_task_bundle_close_594, bundle);
}

void starpu_create_sync_task (starpu_tag_t sync_tag, unsigned ndeps, starpu_tag_t* deps, void (* callback)(void *), void *callback_arg) {
  FUNCTION_ENTRY
	EZTRACE_EVENT_PACKED_3 (EZTRACE_starpu_starpu_create_sync_task_595, sync_tag, ndeps, deps);
  libstarpu_create_sync_task (sync_tag, ndeps, deps, callback, callback_arg);
	EZTRACE_EVENT_PACKED_3 (EZTRACE_starpu_starpu_create_sync_task_596, sync_tag, ndeps, deps);
}

void starpu_execute_on_each_worker (void (* func)(void *), void *arg, uint32_t where) {
    FUNCTION_ENTRY
	EZTRACE_EVENT_PACKED_1 (EZTRACE_starpu_starpu_execute_on_each_worker_691, where);
    libstarpu_execute_on_each_worker (func, arg, where);
    EZTRACE_EVENT_PACKED_1 (EZTRACE_starpu_starpu_execute_on_each_worker_692, where);
}

void starpu_execute_on_each_worker_ex (void (* func)(void *), void *arg, uint32_t where, const char * name) {
  FUNCTION_ENTRY
      EZTRACE_EVENT_PACKED_2 (EZTRACE_starpu_starpu_execute_on_each_worker_ex_693, where, name);
  libstarpu_execute_on_each_worker_ex (func, arg, where, name);
	EZTRACE_EVENT_PACKED_2 (EZTRACE_starpu_starpu_execute_on_each_worker_ex_694, where, name);
}

void starpu_execute_on_specific_workers (void (* func)(void*), void * arg, unsigned num_workers, unsigned * workers, const char * name) {
  FUNCTION_ENTRY
      EZTRACE_EVENT_PACKED_3 (EZTRACE_starpu_starpu_execute_on_specific_workers_695, num_workers, workers, name);
  libstarpu_execute_on_specific_workers (func, arg, num_workers, workers, name);
  EZTRACE_EVENT_PACKED_3 (EZTRACE_starpu_starpu_execute_on_specific_workers_696, num_workers, workers, name);
}

int starpu_data_cpy (starpu_data_handle_t dst_handle, starpu_data_handle_t src_handle, int asynchronous, void (* callback_func)(void*), void *callback_arg) {
	FUNCTION_ENTRY
	EZTRACE_EVENT_PACKED_3 (EZTRACE_starpu_starpu_data_cpy_697, dst_handle, src_handle, asynchronous);
	int ret = libstarpu_data_cpy (dst_handle, src_handle, asynchronous, callback_func, callback_arg);
	EZTRACE_EVENT_PACKED_3 (EZTRACE_starpu_starpu_data_cpy_698, dst_handle, src_handle, asynchronous);
	return ret;
}


START_INTERCEPT_MODULE(starpu)
INTERCEPT2("_starpu_driver_start_job", lib_starpu_driver_start_job)
INTERCEPT2("_starpu_driver_end_job", lib_starpu_driver_end_job)
INTERCEPT2("starpu_cublas_init", libstarpu_cublas_init)
INTERCEPT2("starpu_cublas_shutdown", libstarpu_cublas_shutdown)
INTERCEPT2("starpu_init", libstarpu_init)
INTERCEPT2("starpu_pause", libstarpu_pause)
INTERCEPT2("starpu_resume", libstarpu_resume)
INTERCEPT2("starpu_shutdown", libstarpu_shutdown)
INTERCEPT2("starpu_data_unregister", libstarpu_data_unregister)
INTERCEPT2("starpu_data_unregister_no_coherency", libstarpu_data_unregister_no_coherency)
INTERCEPT2("starpu_data_unregister_submit", libstarpu_data_unregister_submit)
INTERCEPT2("starpu_data_invalidate", libstarpu_data_invalidate)
INTERCEPT2("starpu_data_invalidate_submit", libstarpu_data_invalidate_submit)
INTERCEPT2("starpu_data_acquire", libstarpu_data_acquire)
INTERCEPT2("starpu_data_acquire_on_node", libstarpu_data_acquire_on_node)
INTERCEPT2("starpu_data_acquire_cb", libstarpu_data_acquire_cb)
INTERCEPT2("starpu_data_acquire_on_node_cb", libstarpu_data_acquire_on_node_cb)
INTERCEPT2("starpu_data_release", libstarpu_data_release)
INTERCEPT2("starpu_data_release_on_node", libstarpu_data_release_on_node)
INTERCEPT2("starpu_data_request_allocation", libstarpu_data_request_allocation)
INTERCEPT2("starpu_malloc_on_node", libstarpu_malloc_on_node)
INTERCEPT2("starpu_free_on_node", libstarpu_free_on_node)
INTERCEPT2("starpu_mpi_isend", libstarpu_mpi_isend)
INTERCEPT2("starpu_mpi_irecv", libstarpu_mpi_irecv)
INTERCEPT2("starpu_mpi_send", libstarpu_mpi_send)
INTERCEPT2("starpu_mpi_recv", libstarpu_mpi_recv)
INTERCEPT2("starpu_mpi_isend_detached", libstarpu_mpi_isend_detached)
INTERCEPT2("starpu_mpi_irecv_detached", libstarpu_mpi_irecv_detached)
INTERCEPT2("starpu_mpi_wait", libstarpu_mpi_wait)
INTERCEPT2("starpu_mpi_test", libstarpu_mpi_test)
INTERCEPT2("starpu_mpi_barrier", libstarpu_mpi_barrier)
INTERCEPT2("starpu_mpi_init", libstarpu_mpi_init)
INTERCEPT2("starpu_mpi_initialize", libstarpu_mpi_initialize)
INTERCEPT2("starpu_mpi_initialize_extended", libstarpu_mpi_initialize_extended)
INTERCEPT2("starpu_mpi_shutdown", libstarpu_mpi_shutdown)
INTERCEPT2("starpu_mpi_get_data_on_node", libstarpu_mpi_get_data_on_node)
INTERCEPT2("starpu_mpi_get_data_on_node_detached", libstarpu_mpi_get_data_on_node_detached)
INTERCEPT2("starpu_mpi_redux_data", libstarpu_mpi_redux_data)
INTERCEPT2("starpu_mpi_scatter_detached", libstarpu_mpi_scatter_detached)
INTERCEPT2("starpu_mpi_gather_detached", libstarpu_mpi_gather_detached)
INTERCEPT2("starpu_mpi_isend_detached_unlock_tag", libstarpu_mpi_isend_detached_unlock_tag)
INTERCEPT2("starpu_mpi_irecv_detached_unlock_tag", libstarpu_mpi_irecv_detached_unlock_tag)
INTERCEPT2("starpu_mpi_isend_array_detached_unlock_tag", libstarpu_mpi_isend_array_detached_unlock_tag)
INTERCEPT2("starpu_mpi_irecv_array_detached_unlock_tag", libstarpu_mpi_irecv_array_detached_unlock_tag)
INTERCEPT2("starpu_mpi_comm_amounts_retrieve", libstarpu_mpi_comm_amounts_retrieve)
INTERCEPT2("starpu_mpi_cache_flush", libstarpu_mpi_cache_flush)
INTERCEPT2("starpu_mpi_cache_flush_all_data", libstarpu_mpi_cache_flush_all_data)
#ifndef HAVE_STARPU_MPI_DATA_REGISTER_COMM
INTERCEPT2("starpu_mpi_data_register", libstarpu_mpi_data_register)
#else
INTERCEPT2("starpu_mpi_data_register_comm", libstarpu_mpi_data_register_comm)
#endif
INTERCEPT2("starpu_sched_ctx_add_workers", libstarpu_sched_ctx_add_workers)
INTERCEPT2("starpu_sched_ctx_remove_workers", libstarpu_sched_ctx_remove_workers)
INTERCEPT2("starpu_sched_ctx_delete", libstarpu_sched_ctx_delete)
INTERCEPT2("starpu_sched_ctx_set_inheritor", libstarpu_sched_ctx_set_inheritor)
INTERCEPT2("starpu_sched_ctx_set_context", libstarpu_sched_ctx_set_context)
INTERCEPT2("starpu_sched_ctx_get_context", libstarpu_sched_ctx_get_context)
INTERCEPT2("starpu_sched_ctx_stop_task_submission", libstarpu_sched_ctx_stop_task_submission)
INTERCEPT2("starpu_sched_ctx_finished_submit", libstarpu_sched_ctx_finished_submit)
INTERCEPT2("starpu_sched_ctx_create_worker_collection", libstarpu_sched_ctx_create_worker_collection)
INTERCEPT2("starpu_sched_ctx_delete_worker_collection", libstarpu_sched_ctx_delete_worker_collection)
INTERCEPT2("starpu_sched_ctx_get_worker_collection", libstarpu_sched_ctx_get_worker_collection)
INTERCEPT2("starpu_prefetch_task_input_on_node", libstarpu_prefetch_task_input_on_node)
INTERCEPT2("starpu_malloc", libstarpu_malloc)
INTERCEPT2("starpu_free", libstarpu_free)
INTERCEPT2("starpu_malloc_flags", libstarpu_malloc_flags)
INTERCEPT2("starpu_free_flags", libstarpu_free_flags)
INTERCEPT2("starpu_tag_wait", libstarpu_tag_wait)
INTERCEPT2("starpu_tag_wait_array", libstarpu_tag_wait_array)
INTERCEPT2("starpu_task_init", libstarpu_task_init)
INTERCEPT2("starpu_task_clean", libstarpu_task_clean)
INTERCEPT2("starpu_task_create", libstarpu_task_create)
INTERCEPT2("starpu_task_destroy", libstarpu_task_destroy)
INTERCEPT2("starpu_task_submit", libstarpu_task_submit)
INTERCEPT2("starpu_task_submit_to_ctx", libstarpu_task_submit_to_ctx)
INTERCEPT2("starpu_task_wait", libstarpu_task_wait)
INTERCEPT2("starpu_task_wait_for_all", libstarpu_task_wait_for_all)
INTERCEPT2("starpu_task_wait_for_all_in_ctx", libstarpu_task_wait_for_all_in_ctx)
INTERCEPT2("starpu_task_wait_for_no_ready", libstarpu_task_wait_for_no_ready)
INTERCEPT2("starpu_task_nready", libstarpu_task_nready)
INTERCEPT2("starpu_task_nsubmitted", libstarpu_task_nsubmitted)
INTERCEPT2("starpu_task_dup", libstarpu_task_dup)
INTERCEPT2("starpu_task_bundle_create", libstarpu_task_bundle_create)
INTERCEPT2("starpu_task_bundle_insert", libstarpu_task_bundle_insert)
INTERCEPT2("starpu_task_bundle_remove", libstarpu_task_bundle_remove)
INTERCEPT2("starpu_task_bundle_close", libstarpu_task_bundle_close)
INTERCEPT2("starpu_create_sync_task", libstarpu_create_sync_task)
INTERCEPT2("starpu_execute_on_each_worker", libstarpu_execute_on_each_worker)
INTERCEPT2("starpu_execute_on_each_worker_ex", libstarpu_execute_on_each_worker_ex)
INTERCEPT2("starpu_execute_on_specific_workers", libstarpu_execute_on_specific_workers)
INTERCEPT2("starpu_data_cpy", libstarpu_data_cpy)
INTERCEPT2("starpu_tag_notify_from_apps", libstarpu_tag_notify_from_apps)
END_INTERCEPT_MODULE(starpu)

static void __starpu_init (void) __attribute__ ((constructor));
/* Initialize the current library */
static void
__starpu_init (void)
{
  DYNAMIC_INTERCEPT_ALL_MODULE(starpu);

  /* start event recording */
#ifdef EZTRACE_AUTOSTART
  eztrace_start ();
#endif



}

static void __starpu_conclude (void) __attribute__ ((destructor));
static void
__starpu_conclude (void)
{
  /* stop event recording */
  eztrace_stop ();
}
