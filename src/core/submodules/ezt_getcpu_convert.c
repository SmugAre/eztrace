#include <GTG.h>
#include "eztrace_convert.h"
#include "ezt_submodule.h"
#include "ezt_getcpu.h"


/* These functions are called post mortem (by eztrace_convert/eztrace_stats) */
int ezt_getcpu_convert_init() {
  addVarType ("V_CURCPU", "Current CPU", "CT_Thread");

  return 0;
}

int ezt_getcpu_convert_events(eztrace_event_t *ev) {

  DECLARE_THREAD_ID_STR(thread_id, CUR_INDEX, CUR_THREAD_ID);
  switch (LITL_READ_GET_CODE(ev)) {
  case EZT_GETCPU:
    {
      /* todo */
      FUNC_NAME;
      int cpu = GET_PARAM(CUR_EV, 1);
      setVar(CURRENT, "V_CURCPU", thread_id, (double)cpu);
      break;
    }
  default:
    return 0;
  }

  return 1;
}


int ezt_getcpu_convert_stats(eztrace_event_t *ev __attribute__((unused))) {
  return 0;
}

void ezt_getcpu_convert_print_stats() {
}
